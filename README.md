# Timegrid Website Public Distribution

The Public Distribution of the HexaFlexa Timegrid Website.

## Redirects
For GitLab Pages, add a *_redirects* file in the *public* folder, to handle Angular redirects.

```
/* /index.html 200
```

When deploying locally, can handle redirects from command line arguments with the `--proxy http://127.0.0.1:8090?` argument:

```shell
http-server --port 8090 --proxy http://127.0.0.1:8090?
```
