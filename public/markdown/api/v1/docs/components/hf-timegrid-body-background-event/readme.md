# hf-timegrid-body-background-event



<!-- Auto Generated Below -->


## Overview

The timegrid body background event component, used internally by `hf-timegrid` component.
This component is used to render the background event in the body.
It is used in the `hf-timegrid-body` component.

## Properties

| Property                  | Attribute            | Description                        | Type            | Default     |
| ------------------------- | -------------------- | ---------------------------------- | --------------- | ----------- |
| `backgroundEventConfig`   | --                   | The background event configuration | `HfEventConfig` | `undefined` |
| `columnResourceId`        | `column-resource-id` | The column resource id             | `string`        | `undefined` |
| `event` _(required)_      | --                   | The event                          | `HfEvent`       | `undefined` |
| `timeFormat` _(required)_ | `time-format`        | The time format                    | `string`        | `undefined` |


## Shadow Parts

| Part                                  | Description                      |
| ------------------------------------- | -------------------------------- |
| `"body-background-event-content"`     | The background event content     |
| `"body-background-event-description"` | The background event description |
| `"body-background-event-time"`        | The background event time        |
| `"body-background-event-title"`       | The background event title       |
| `"body-event-title"`                  |                                  |


## CSS Custom Properties

| Name              | Description             |
| ----------------- | ----------------------- |
| `--background`    | Event background color. |
| `--border`        | Event border.           |
| `--border-radius` | Event border radius.    |
| `--color`         | Event text color.       |


## Dependencies

### Used by

 - [hf-timegrid-body](../hf-timegrid-body)

### Graph
```mermaid
graph TD;
  hf-timegrid-body --> hf-timegrid-body-background-event
  style hf-timegrid-body-background-event fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
