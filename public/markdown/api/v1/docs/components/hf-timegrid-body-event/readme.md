# hf-timegrid-body-event



<!-- Auto Generated Below -->


## Overview

The timegrid body event component, used internally by `hf-timegrid` component.
This component is used to render the event in the body.
It is used in the `hf-timegrid-body` component.

## Properties

| Property                  | Attribute            | Description                                     | Type            | Default     |
| ------------------------- | -------------------- | ----------------------------------------------- | --------------- | ----------- |
| `alwaysShowResize`        | `always-show-resize` | The always show resize flag. Defaults to false. | `boolean`       | `undefined` |
| `columnResourceId`        | `column-resource-id` | The column resource id                          | `string`        | `undefined` |
| `dragActive`              | `drag-active`        | The drag active flag. Defaults to false         | `boolean`       | `undefined` |
| `dragResizeEnd`           | `drag-resize-end`    | The drag resize end time                        | `string`        | `undefined` |
| `dragResizeStart`         | `drag-resize-start`  | The drag resize start time                      | `string`        | `undefined` |
| `event` _(required)_      | --                   | The event                                       | `HfEvent`       | `undefined` |
| `eventConfig`             | --                   | The event configuration                         | `HfEventConfig` | `undefined` |
| `isNewEvent`              | `is-new-event`       | The new event flag. Defaults to false           | `boolean`       | `undefined` |
| `noSwiping`               | `no-swiping`         | The no swiping flag. Defaults to false          | `boolean`       | `undefined` |
| `resizeActive`            | `resize-active`      | The resize active flag. Defaults to false       | `boolean`       | `undefined` |
| `timeFormat` _(required)_ | `time-format`        | The time format                                 | `string`        | `undefined` |


## Shadow Parts

| Part                         | Description             |
| ---------------------------- | ----------------------- |
| `"body-event-content"`       | The event content       |
| `"body-event-description"`   | The event description   |
| `"body-event-resize-bottom"` | The event resize bottom |
| `"body-event-resize-top"`    | The event resize top    |
| `"body-event-time"`          | The event time          |
| `"body-event-title"`         | The event title         |


## CSS Custom Properties

| Name                                | Description                                                   |
| ----------------------------------- | ------------------------------------------------------------- |
| `--background`                      | Event background color.                                       |
| `--border`                          | Event border.                                                 |
| `--border-radius`                   | Event border radius.                                          |
| `--color`                           | Event text color.                                             |
| `--draggable-background`            | Event background color when draggable.                        |
| `--draggable-border`                | Event border when draggable.                                  |
| `--draggable-color`                 | Event text color when draggable.                              |
| `--draggable-resizeable-background` | Event background color when draggable and resizeable.         |
| `--draggable-resizeable-border`     | Event border when draggable and resizeable.                   |
| `--draggable-resizeable-color`      | Event text color when draggable and resizeable.               |
| `--new-background`                  | Event placeholder background color when creating a new event. |
| `--new-border`                      | Event placeholder border when creating a new event.           |
| `--new-color`                       | Event placeholder text color when creating a new event.       |
| `--resize-background`               | Event resize handle background color.                         |
| `--resize-border`                   | Event resize handle border.                                   |
| `--resizeable-background`           | Event background color when resizeable.                       |
| `--resizeable-border`               | Event border when resizeable.                                 |
| `--resizeable-color`                | Event text color when resizeable.                             |


## Dependencies

### Used by

 - [hf-timegrid-body](../hf-timegrid-body)

### Graph
```mermaid
graph TD;
  hf-timegrid-body --> hf-timegrid-body-event
  style hf-timegrid-body-event fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
