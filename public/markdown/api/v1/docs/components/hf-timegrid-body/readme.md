# hf-timegrid-body



<!-- Auto Generated Below -->


## Overview

The timegrid body component, used internally by `hf-timegrid` component.
This component is used to render the timegrid body in a slide.
It is used in the `hf-timegrid-slide` component.

## Properties

| Property                         | Attribute             | Description                            | Type                | Default     |
| -------------------------------- | --------------------- | -------------------------------------- | ------------------- | ----------- |
| `bodyConfig`                     | --                    | The timegrid grid configuration object | `HfBodyConfig`      | `undefined` |
| `currentSlideIndex` _(required)_ | `current-slide-index` | The current slide index                | `number`            | `undefined` |
| `resources` _(required)_         | --                    | The resources                          | `HfResource[]`      | `undefined` |
| `slideIndex` _(required)_        | `slide-index`         | The slide index                        | `number`            | `undefined` |
| `timeFormat` _(required)_        | `time-format`         | The time format                        | `string`            | `undefined` |
| `timegridGroups` _(required)_    | --                    | The timegrid groups                    | `HfTimegridGroup[]` | `undefined` |


## Events

| Event                             | Description                                                                                                                                                 | Type                                                      |
| --------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------- |
| `bodyEventChanged`                | An event raised when the user changes an event: drag or resize.                                                                                             | `CustomEvent<HfTimegridEventChangedEvent>`                |
| `bodyEventDragResizeStateChanged` | An event raised when the user changes the timegrid event drag-resize state. The possible drag-resize states are: 'drag', 'resize', 'dragResize' and 'none'. | `CustomEvent<HfTimegridEventDragResizeStateChangedEvent>` |
| `bodyEventNew`                    | An event raised when the user creates a new event.                                                                                                          | `CustomEvent<HfEvent>`                                    |
| `bodyEventSelected`               | An event raised when the user selects (taps or holds) a timegrid event.                                                                                     | `CustomEvent<HfEvent>`                                    |
| `gridScrollbarSizeChanged`        | (optional) Signal that the grid scrollbar size changed.                                                                                                     | `CustomEvent<HfTimegridScrollbarSizeEvent>`               |
| `gridScrollChanged`               | (optional) Signal that the grid was scrolled.                                                                                                               | `CustomEvent<HfTimegridScrollEvent>`                      |
| `zoomGrid`                        | An event raised when the user zooms the grid.                                                                                                               | `CustomEvent<HfZoomGridEvent>`                            |


## Methods

### `clean() => Promise<void>`

Cleans the timegrid body.

#### Returns

Type: `Promise<void>`



### `refresh() => Promise<void>`

Refresh the component.

#### Returns

Type: `Promise<void>`



### `refreshEvent(eventId: string) => Promise<void>`

Refresh the event.

#### Parameters

| Name      | Type     | Description |
| --------- | -------- | ----------- |
| `eventId` | `string` |             |

#### Returns

Type: `Promise<void>`



### `updateGridScroll(scrollTop: number, scrollLeft: number, updateOnlyScroll?: boolean) => Promise<void>`

Updates the timegrid body scroll.

#### Parameters

| Name               | Type      | Description                    |
| ------------------ | --------- | ------------------------------ |
| `scrollTop`        | `number`  | the scroll top                 |
| `scrollLeft`       | `number`  | the scroll left                |
| `updateOnlyScroll` | `boolean` | true to update only the scroll |

#### Returns

Type: `Promise<void>`



### `updateGridZoom(zoomCellMinWidth: number, zoomCellMinHeight: number) => Promise<void>`

Updates the timegrid grid zoom.

#### Parameters

| Name                | Type     | Description              |
| ------------------- | -------- | ------------------------ |
| `zoomCellMinWidth`  | `number` | the zoom cell min width  |
| `zoomCellMinHeight` | `number` | the zoom cell min height |

#### Returns

Type: `Promise<void>`




## Shadow Parts

| Part                      | Description                                |
| ------------------------- | ------------------------------------------ |
| `"body-background-event"` | The timegrid body background event         |
| `"body-event"`            | The timegrid body event                    |
| `"body-event-new"`        | The timegrid body new event                |
| `"body-grid"`             | The timegrid body grid                     |
| `"body-time"`             | The timegrid body time                     |
| `"body-time-label"`       | The timegrid body time label               |
| `"~exported parts~"`      | Exports the `hf-timegrid-body-event` parts |


## CSS Custom Properties

| Name                                       | Description                                   |
| ------------------------------------------ | --------------------------------------------- |
| `--grid-background`                        | Grid background color.                        |
| `--grid-dropzone-active-background`        | Grid dropzone active background color.        |
| `--grid-dropzone-active-reject-background` | Grid dropzone active reject background color. |
| `--grid-dropzone-enter-background`         | Grid dropzone enter background color.         |
| `--grid-dropzone-enter-reject-background`  | Grid dropzone enter reject background color.  |
| `--grid-horizontal-border`                 | Grid horizontal border.                       |
| `--grid-vertical-border`                   | Grid vertical border.                         |
| `--grid-vertical-group-border`             | Grid vertical group border.                   |
| `--time-background`                        | Time background color.                        |
| `--time-grid-vertical-border`              | Time grid vertical border.                    |
| `--time-horizontal-border`                 | Time horizontal border.                       |
| `--time-indicator-background`              | Time indicator background color.              |
| `--time-indicator-color`                   | Time indicator color.                         |
| `--time-label-color`                       | Time label color.                             |


## Dependencies

### Used by

 - [hf-timegrid-slide](../hf-timegrid-slide)

### Depends on

- [hf-timegrid-body-background-event](../hf-timegrid-body-background-event)
- [hf-timegrid-body-event](../hf-timegrid-body-event)

### Graph
```mermaid
graph TD;
  hf-timegrid-body --> hf-timegrid-body-background-event
  hf-timegrid-body --> hf-timegrid-body-event
  hf-timegrid-slide --> hf-timegrid-body
  style hf-timegrid-body fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
