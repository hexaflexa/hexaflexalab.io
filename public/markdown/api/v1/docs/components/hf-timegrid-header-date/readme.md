# hf-timegrid-header-date



<!-- Auto Generated Below -->


## Overview

The timegrid header date component, used internally by `hf-timegrid` component.
This component is used to render the date in the header.
It is used in the `hf-timegrid-header` component.

## Properties

| Property                   | Attribute    | Description                  | Type                | Default     |
| -------------------------- | ------------ | ---------------------------- | ------------------- | ----------- |
| `date` _(required)_        | `date`       | The date                     | `string`            | `undefined` |
| `headerDayConfig`          | --           | The header day configuration | `HfHeaderDayConfig` | `undefined` |
| `headerRow` _(required)_   | `header-row` | The header row               | `number`            | `undefined` |
| `weekdayInfo` _(required)_ | --           | The weekday info             | `HfWeekdayInfo`     | `undefined` |


## Methods

### `updateDisplayLayout() => Promise<void>`

Updates the display layout.

#### Returns

Type: `Promise<void>`




## Shadow Parts

| Part                       | Description                        |
| -------------------------- | ---------------------------------- |
| `"header-date-day"`        | The header date day                |
| `"header-date-day-0"`      | The header date day for Sunday     |
| `"header-date-day-1"`      | The header date day for Monday     |
| `"header-date-day-2"`      | The header date day for Tuesday    |
| `"header-date-day-3"`      | The header date day for Wednesday  |
| `"header-date-day-4"`      | The header date day for Thursday   |
| `"header-date-day-5"`      | The header date day for Friday     |
| `"header-date-day-6"`      | The header date day for Saturday   |
| `"header-date-day-today"`  | The header date day for today      |
| `"header-date-name"`       | The header date name               |
| `"header-date-name-0"`     | The header date name for Sunday    |
| `"header-date-name-1"`     | The header date name for Monday    |
| `"header-date-name-2"`     | The header date name for Tuesday   |
| `"header-date-name-3"`     | The header date name for Wednesday |
| `"header-date-name-4"`     | The header date name for Thursday  |
| `"header-date-name-5"`     | The header date name for Friday    |
| `"header-date-name-6"`     | The header date name for Saturday  |
| `"header-date-name-today"` | The header date name for today     |


## Dependencies

### Used by

 - [hf-timegrid-header](../hf-timegrid-header)

### Graph
```mermaid
graph TD;
  hf-timegrid-header --> hf-timegrid-header-date
  style hf-timegrid-header-date fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
