# hf-timegrid-header-resource



<!-- Auto Generated Below -->


## Overview

The timegrid header resource component, used internally by `hf-timegrid` component.
This component is used to render the resource in the header.
It is used in the `hf-timegrid-header` component.

## Properties

| Property                  | Attribute    | Description                | Type                     | Default     |
| ------------------------- | ------------ | -------------------------- | ------------------------ | ----------- |
| `headerResourceConfig`    | --           | The resource configuration | `HfHeaderResourceConfig` | `undefined` |
| `headerRow` _(required)_  | `header-row` | The header row             | `number`                 | `undefined` |
| `hfResource` _(required)_ | --           | The resource               | `HfResource`             | `undefined` |


## Methods

### `updateDisplayLayout() => Promise<void>`

Updates the display layout.

#### Returns

Type: `Promise<void>`




## Shadow Parts

| Part                       | Description                |
| -------------------------- | -------------------------- |
| `"header-resource-avatar"` | The header resource avatar |
| `"header-resource-image"`  | The header resource image  |
| `"header-resource-title"`  | The header resource title  |


## CSS Custom Properties

| Name                 | Description            |
| -------------------- | ---------------------- |
| `--background-color` | The background color.  |
| `--colorbar-height`  | Time color bar height. |


## Dependencies

### Used by

 - [hf-timegrid-header](../hf-timegrid-header)

### Graph
```mermaid
graph TD;
  hf-timegrid-header --> hf-timegrid-header-resource
  style hf-timegrid-header-resource fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
