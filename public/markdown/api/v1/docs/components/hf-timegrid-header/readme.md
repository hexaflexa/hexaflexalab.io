# hf-timegrid-header



<!-- Auto Generated Below -->


## Overview

The timegrid header component, used internally by `hf-timegrid` component.
This component is used to render the timegrid header in a slide.
It is used in the `hf-timegrid-slide` component.

## Properties

| Property                      | Attribute     | Description                       | Type                | Default     |
| ----------------------------- | ------------- | --------------------------------- | ------------------- | ----------- |
| `config`                      | --            | The timegrid configuration object | `HfTimegridConfig`  | `undefined` |
| `slideIndex` _(required)_     | `slide-index` | The slide index                   | `number`            | `undefined` |
| `timegridGroups` _(required)_ | --            | The timegrid groups               | `HfTimegridGroup[]` | `undefined` |
| `weekdayInfos` _(required)_   | --            |                                   | `HfWeekdayInfo[]`   | `undefined` |


## Methods

### `updateGridScroll(scrollLeft: number) => Promise<void>`

Updates the grid scroll.

#### Parameters

| Name         | Type     | Description     |
| ------------ | -------- | --------------- |
| `scrollLeft` | `number` | the scroll left |

#### Returns

Type: `Promise<void>`



### `updateGridZoom(zoomCellMinWidth: number) => Promise<void>`

Updates the timegrid grid zoom.

#### Parameters

| Name               | Type     | Description             |
| ------------------ | -------- | ----------------------- |
| `zoomCellMinWidth` | `number` | the zoom cell min width |

#### Returns

Type: `Promise<void>`



### `updateScrollbarPlaceholderWidth(scrollbarWidth: number) => Promise<void>`

Updates the scrollbar placeholder width.

#### Parameters

| Name             | Type     | Description         |
| ---------------- | -------- | ------------------- |
| `scrollbarWidth` | `number` | the scrollbar width |

#### Returns

Type: `Promise<void>`




## Shadow Parts

| Part                 | Description                                           |
| -------------------- | ----------------------------------------------------- |
| `"header-date"`      | The header date                                       |
| `"header-resource"`  | The header resource                                   |
| `"~exported parts~"` | Exports the `header-resource` and `header-date` parts |


## CSS Custom Properties

| Name                           | Description                                                                                                          |
| ------------------------------ | -------------------------------------------------------------------------------------------------------------------- |
| `--bottom-border`              | Bottom border.                                                                                                       |
| `--grid-background`            | Grid background color.                                                                                               |
| `--grid-horizontal-border`     | Grid horizontal border.                                                                                              |
| `--grid-vertical-border`       | Grid vertical border.                                                                                                |
| `--grid-vertical-group-border` | Grid vertical group border.                                                                                          |
| `--grid-vertical-right-border` | Grid vertical right border.                                                                                          |
| `--scrollbar-background`       | Scrollbar background color. The scrollbar is the right section of the header, above the grid scrollbar (when shown). |
| `--time-cell-background`       | Time cell background color. The time cell is the left section of the header, above the time column of the body.      |
| `--time-grid-vertical-border`  | Time grid vertical border.                                                                                           |
| `--time-label-color`           | Time label color.                                                                                                    |


## Dependencies

### Used by

 - [hf-timegrid-slide](../hf-timegrid-slide)

### Depends on

- [hf-timegrid-header-resource](../hf-timegrid-header-resource)
- [hf-timegrid-header-date](../hf-timegrid-header-date)

### Graph
```mermaid
graph TD;
  hf-timegrid-header --> hf-timegrid-header-resource
  hf-timegrid-header --> hf-timegrid-header-date
  hf-timegrid-slide --> hf-timegrid-header
  style hf-timegrid-header fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
