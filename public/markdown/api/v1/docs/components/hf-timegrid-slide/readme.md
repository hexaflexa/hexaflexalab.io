# hf-timegrid-slide



<!-- Auto Generated Below -->


## Overview

The timegrid slide component, used internally by `hf-timegrid` component.
This component is used to render the slide.

## Properties

| Property                             | Attribute             | Description                       | Type                                                                                                      | Default     |
| ------------------------------------ | --------------------- | --------------------------------- | --------------------------------------------------------------------------------------------------------- | ----------- |
| `backgroundEventSource` _(required)_ | --                    | The background event source       | `(dates: string[], callback: (events: HfEvent[]) => void, failureCallback: (error: any) => void) => void` | `undefined` |
| `config`                             | --                    | The timegrid configuration object | `HfTimegridConfig`                                                                                        | `undefined` |
| `currentSlideIndex` _(required)_     | `current-slide-index` | The current slide index           | `number`                                                                                                  | `undefined` |
| `dates` _(required)_                 | --                    | The dates                         | `string[]`                                                                                                | `undefined` |
| `eventSource` _(required)_           | --                    | The event source                  | `(dates: string[], callback: (events: HfEvent[]) => void, failureCallback: (error: any) => void) => void` | `undefined` |
| `groupByResources` _(required)_      | `group-by-resources`  | The group by resources flag       | `boolean`                                                                                                 | `undefined` |
| `resources` _(required)_             | --                    | The resources                     | `HfResource[]`                                                                                            | `undefined` |
| `slideIndex` _(required)_            | `slide-index`         | The slide index                   | `number`                                                                                                  | `undefined` |
| `weekdayInfos` _(required)_          | --                    | The weekday infos                 | `HfWeekdayInfo[]`                                                                                         | `undefined` |


## Methods

### `clean() => Promise<void>`

Cleans the grid.

#### Returns

Type: `Promise<void>`



### `refreshEvent(eventId: string) => Promise<void>`

Refresh the event.

#### Parameters

| Name      | Type     | Description |
| --------- | -------- | ----------- |
| `eventId` | `string` |             |

#### Returns

Type: `Promise<void>`



### `updateGridScroll(scrollTop: number, scrollLeft: number, updateOnlyScroll?: boolean) => Promise<void>`

Updates the timegrid grid scroll.

#### Parameters

| Name               | Type      | Description                    |
| ------------------ | --------- | ------------------------------ |
| `scrollTop`        | `number`  | the scroll top                 |
| `scrollLeft`       | `number`  | the scroll left                |
| `updateOnlyScroll` | `boolean` | true to update only the scroll |

#### Returns

Type: `Promise<void>`



### `updateGridZoom(zoomCellMinWidth: number, zoomCellMinHeight: number) => Promise<void>`

Updates the timegrid grid zoom.

#### Parameters

| Name                | Type     | Description              |
| ------------------- | -------- | ------------------------ |
| `zoomCellMinWidth`  | `number` | the zoom cell min width  |
| `zoomCellMinHeight` | `number` | the zoom cell min height |

#### Returns

Type: `Promise<void>`




## Shadow Parts

| Part                 | Description                                                   |
| -------------------- | ------------------------------------------------------------- |
| `"body"`             | The body                                                      |
| `"header"`           | The header                                                    |
| `"~exported parts~"` | Exports the `hf-timegrid-header` and `hf-timegrid-body` parts |


## Dependencies

### Used by

 - [hf-timegrid](../hf-timegrid)

### Depends on

- [hf-timegrid-header](../hf-timegrid-header)
- [hf-timegrid-body](../hf-timegrid-body)

### Graph
```mermaid
graph TD;
  hf-timegrid-slide --> hf-timegrid-header
  hf-timegrid-slide --> hf-timegrid-body
  hf-timegrid-header --> hf-timegrid-header-resource
  hf-timegrid-header --> hf-timegrid-header-date
  hf-timegrid-body --> hf-timegrid-body-background-event
  hf-timegrid-body --> hf-timegrid-body-event
  hf-timegrid --> hf-timegrid-slide
  style hf-timegrid-slide fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
