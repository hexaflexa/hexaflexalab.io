# hf-timegrid-toolbar



<!-- Auto Generated Below -->


## Overview

The timegrid toolbar component, used internally by `hf-timegrid` component.
This component is used to render the toolbar.

## Properties

| Property             | Attribute | Description                       | Type               | Default     |
| -------------------- | --------- | --------------------------------- | ------------------ | ----------- |
| `config`             | --        | The timegrid configuration object | `HfTimegridConfig` | `null`      |
| `dates` _(required)_ | --        | The dates                         | `string[]`         | `undefined` |


## Events

| Event           | Description                                                                            | Type                  |
| --------------- | -------------------------------------------------------------------------------------- | --------------------- |
| `dateChanged`   | The date changed event. The event detail is the date as a string in yyyy-MM-dd format. | `CustomEvent<string>` |
| `nextClick`     | The next date button click event.                                                      | `CustomEvent<void>`   |
| `previousClick` | The previous date button click event.                                                  | `CustomEvent<void>`   |
| `todayClick`    | The today button click event.                                                          | `CustomEvent<void>`   |


## Methods

### `hideLoading() => Promise<void>`

Hide the loading icon.

#### Returns

Type: `Promise<void>`



### `showLoading() => Promise<void>`

Show the loading icon.

#### Returns

Type: `Promise<void>`




## Slots

| Slot              | Description                                                   |
| ----------------- | ------------------------------------------------------------- |
| `"center-after"`  | Slot at the center of the toolbar, after the toolbar buttons  |
| `"center-before"` | Slot at the center of the toolbar, before the toolbar buttons |
| `"end-after"`     | Slot at the end of the toolbar, after the toolbar buttons     |
| `"end-before"`    | Slot at the end of the toolbar, before the toolbar buttons    |
| `"start-after"`   | Slot at the start of the toolbar, after the toolbar buttons   |
| `"start-before"`  | Slot at the start of the toolbar, before the toolbar buttons  |


## Shadow Parts

| Part                | Description                 |
| ------------------- | --------------------------- |
| `"toolbar-button"`  | Each of the toolbar buttons |
| `"toolbar-center"`  | The toolbar center          |
| `"toolbar-end"`     | The toolbar end             |
| `"toolbar-loading"` | The toolbar loading icon    |
| `"toolbar-start"`   | The toolbar start           |


## Dependencies

### Used by

 - [hf-timegrid](../hf-timegrid)

### Graph
```mermaid
graph TD;
  hf-timegrid --> hf-timegrid-toolbar
  style hf-timegrid-toolbar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
