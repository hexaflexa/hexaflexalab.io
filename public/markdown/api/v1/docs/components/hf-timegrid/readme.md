# hf-timegrid



<!-- Auto Generated Below -->


## Overview

The timegrid component, to be used in your template, JSX, html etc.

## Properties

| Property    | Attribute    | Description                       | Type               | Default      |
| ----------- | ------------ | --------------------------------- | ------------------ | ------------ |
| `config`    | --           | The timegrid configuration object | `HfTimegridConfig` | `undefined`  |
| `startDate` | `start-date` | The start date                    | `string`           | `getToday()` |


## Events

| Event                         | Description                                                                                                                                                 | Type                                                      |
| ----------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------- |
| `eventChanged`                | An event raised when the user changes an event: drag or resize.                                                                                             | `CustomEvent<HfTimegridEventChangedEvent>`                |
| `eventDragResizeStateChanged` | An event raised when the user changes the timegrid event drag-resize state. The possible drag-resize states are: 'drag', 'resize', 'dragResize' and 'none'. | `CustomEvent<HfTimegridEventDragResizeStateChangedEvent>` |
| `eventNew`                    | An event raised when the user creates a new event.                                                                                                          | `CustomEvent<HfEvent>`                                    |
| `eventSelected`               | An event raised when the user selects (taps or holds) a timegrid event.                                                                                     | `CustomEvent<HfEvent>`                                    |
| `startDateChanged`            | An event raised when the start date is changed.                                                                                                             | `CustomEvent<string>`                                     |


## Methods

### `hideLoading() => Promise<void>`

Hide the loading icon.

#### Returns

Type: `Promise<void>`



### `refresh() => Promise<void>`

Refresh the component.

#### Returns

Type: `Promise<void>`



### `refreshEvent(event: HfEvent | string) => Promise<void>`

Refresh the event.

#### Parameters

| Name    | Type                | Description |
| ------- | ------------------- | ----------- |
| `event` | `string \| HfEvent` |             |

#### Returns

Type: `Promise<void>`



### `showLoading() => Promise<void>`

Show the loading icon.

#### Returns

Type: `Promise<void>`




## Slots

| Slot                      | Description                                                   |
| ------------------------- | ------------------------------------------------------------- |
| `"toolbar-center-after"`  | Slot at the center of the toolbar, after the toolbar buttons  |
| `"toolbar-center-before"` | Slot at the center of the toolbar, before the toolbar buttons |
| `"toolbar-end-after"`     | Slot at the end of the toolbar, after the toolbar buttons     |
| `"toolbar-end-before"`    | Slot at the end of the toolbar, before the toolbar buttons    |
| `"toolbar-start-after"`   | Slot at the start of the toolbar, after the toolbar buttons   |
| `"toolbar-start-before"`  | Slot at the start of the toolbar, before the toolbar buttons  |


## Shadow Parts

| Part                 | Description                                                     |
| -------------------- | --------------------------------------------------------------- |
| `"slide"`            | The slide                                                       |
| `"toolbar"`          | The toolbar                                                     |
| `"~exported parts~"` | Exports the `hf-timegrid-toolbar` and `hf-timegrid-slide` parts |


## Dependencies

### Depends on

- [hf-timegrid-toolbar](../hf-timegrid-toolbar)
- [hf-timegrid-slide](../hf-timegrid-slide)

### Graph
```mermaid
graph TD;
  hf-timegrid --> hf-timegrid-toolbar
  hf-timegrid --> hf-timegrid-slide
  hf-timegrid-slide --> hf-timegrid-header
  hf-timegrid-slide --> hf-timegrid-body
  hf-timegrid-header --> hf-timegrid-header-resource
  hf-timegrid-header --> hf-timegrid-header-date
  hf-timegrid-body --> hf-timegrid-body-background-event
  hf-timegrid-body --> hf-timegrid-body-event
  style hf-timegrid fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
