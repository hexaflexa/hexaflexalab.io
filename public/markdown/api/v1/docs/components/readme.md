# Timegrid Components

The `hf-timegrid` component should be used directly in your template, JSX, html etc.

The other components are used internally by `hf-timegrid` and are not meant to be used directly.

## Components

- [hf-timegrid](./docs/components/hf-timegrid)
- [hf-timegrid-toolbar](./docs/components/hf-timegrid-toolbar)
- [hf-timegrid-slide](./docs/components/hf-timegrid-slide)
- [hf-timegrid-header](./docs/components/hf-timegrid-header)
- [hf-timegrid-header-date](./docs/components/hf-timegrid-header-date)
- [hf-timegrid-header-resource](./docs/components/hf-timegrid-header-resource)
- [hf-timegrid-body](./docs/components/hf-timegrid-body)
- [hf-timegrid-body-event](./docs/components/hf-timegrid-body-event)

## Graph
```mermaid
graph TD;
  hf-timegrid --> hf-timegrid-toolbar
  hf-timegrid --> hf-timegrid-slide
  hf-timegrid-slide --> hf-timegrid-header
  hf-timegrid-slide --> hf-timegrid-body
  hf-timegrid-header --> hf-timegrid-header-date
  hf-timegrid-header --> hf-timegrid-header-resource
  hf-timegrid-body --> hf-timegrid-body-event
  style hf-timegrid fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Timegrid web component made with 💗 by HexaFlexa!*
