# Model classes

The model classes used by `hf-timegrid` web component:

```mermaid
graph TD;
  HfTimegridConfig --> HfDaysConfig
  HfTimegridConfig -->|resources| HfResource
  HfTimegridConfig -->|events| HfEvent
  HfTimegridConfig -->|backgroundEvents| HfEvent
  HfTimegridConfig -->|eventSource| HfEventSource
  HfTimegridConfig -->|backgroundEventSource| HfEventSource
  HfTimegridConfig --> HfToolbarConfig
  HfTimegridConfig --> HfHeaderDayConfig
  HfTimegridConfig --> HfHeaderResourceConfig
  HfTimegridConfig --> HfBodyConfig
  HfBodyConfig --> HfEventConfig
```

```typescript
/**
 * The timegrid configuration model.
 */
export class HfTimegridConfig {
  /** The days configuration. */
  daysConfig?: HfDaysConfig;
  /** The resources. */
  resources?: HfResource[];
  /** The events. */
  events?: HfEvent[];
  /** The source of events. If provided, it takes precedence over the events array. */
  eventSource?: HfEventSource;
  /** The background events. */
  backgroundEvents?: HfEvent[];
  /** The source of background events. If provided, it takes precedence over the background events array. */
  backgroundEventSource?: HfEventSource;
  /** The locale. Used to display the month names and weekday names. */
  locale?: string;
  /** The index of the first day of the week. Defaults to Sunday - 0. */
  firstDayOfWeek?: number;
  /** Whether to group the events by resources. Defaults to false. */
  groupByResources?: boolean;
  /** Whether to display the toolbar. Defaults to true. */
  showToolbar?: boolean;
  /** The toolbar configuration. */
  toolbarConfig?: HfToolbarConfig;
  /** The header day configuration. */
  headerDayConfig?: HfHeaderDayConfig;
  /** The header resource configuration. */
  headerResourceConfig?: HfHeaderResourceConfig;
  /** The time format. Valid values: 'HH:mm' and 'hh:mm a'. Defaults to 'HH:mm'. */
  timeFormat?: string;
  /** The disable swipe flag. Defaults to false. */
  disableSwiping?: boolean;
  /** Whether to show the Powered by mention. Default to true. */
  showPoweredBy?: boolean;
  /** The timegrid grid configuration. */
  bodyConfig?: HfBodyConfig;
}

/**
 * The days configuration.
 */
export class HfDaysConfig {
  /** Whether to display the full week. Defaults to false. */
  fullWeek?: boolean;
  /** The number of days to display. Defaults to 1. */
  daysCount?: number;
  /** Whether to include the inactive days. Defaults to true. */
  includeInactiveDays?: boolean;
  /**
   * The active day checker.
   * @param date the date string in format yyyy-MM-dd
   * @returns true if the day is active, false otherwise
   */
  activeDayChecker?: (date: string) => boolean;
}

/**
 * The resource model.
 */
export class HfResource {
  /** The resource id. */
  id: string;
  /** The resource title. */
  title: string;
  /** The resource color. */
  backgroundColor?: string;
  /** The resource image source. */
  imageSrc?: string;
  /** The resource extended properties. */
  extendedProps?: any;
  /** Whether the resource events are allowed to overlap with other events. Defaults to true. */
  eventOverlap?: boolean;
}

/**
 * The event model.
 */
export class HfEvent {
  /** The event id. */
  id: string;
  /** The group id. */
  groupId?: string;
  /** The event title. */
  title?: string;
  /** The event description. */
  description?: string;
  /** The start date and time in 'yyyy-MM-dd HH:mm' format. */
  start: string;
  /** The end date and time in 'yyyy-MM-dd HH:mm' format. */
  end: string;
  /** The array of resource ids the event is associated with. */
  resources?: string[];
  /** The event custom style. */
  style?: any;
  /** Whether the event is allowed to overlap with other events. Defaults to true. */
  overlap?: boolean;
  /** The event extended properties. */
  extendedProps?: any;
  /** The event display. Possible values: 'default', 'none', 'background' */
  display?: HfEventDisplay;
  /** The event interact configuration. */
  dragResize?: HfEventDragResize;
}

/**
 * The event display type.
 */
export type HfEventDisplay = "default" | "none";

/**
 * The event interact type.
 */
export class HfEventDragResize {
  /** Whether the event is draggable. Defaults to true. */
  draggable?: boolean;
  /** Whether the event is resizable. Defaults to true. */
  resizable?: boolean;
  /** The resize edges. Defaults to 'both'. */
  resizeEdges?: HfEventResizeEdges;
};

/**
 * The event resize action.
 */
export type HfEventResizeEdges = "both" | "start" | "end";

/**
 * The event source function signature. The events should be passed to the callback.
 * 
 * @param dates the dates for which to fetch the events
 * @param callback the callback to be called with the fetched events
 * @param failureCallback the callback to be called in case of failure
 */
export type HfEventSource = (dates: string[], callback: (events: HfEvent[]) => void, failureCallback: (error: any) => void) => void;

/**
 * The toolbar configuration.
 * The accepted controls are: 'prev', 'next', 'today', 'date', 'loading'.
 */
export class HfToolbarConfig {
  /** The controls to be displayed at the start. */
  startControls?: HfToolbarControl[];
  /** The controls to be displayed at the center. */
  centerControls?: HfToolbarControl[];
  /** The controls to be displayed at the end. */
  endControls?: HfToolbarControl[];
  /** Whether to disable the date calendar of the 'date' control. Defaults to false. */
  disableDateCalendar?: boolean;
}

/**
 * The toolbar controls.
 */
export type HfToolbarControl = "prev" | "next" | "today" | "date" | "loading";

/**
 * The header day configuration.
 */
export class HfHeaderDayConfig {
  /** Whether to display the date. Defaults to true. */
  showDate?: boolean;
  /** Whether to display the weekday. Defaults to true. */
  showWeekday?: boolean;
  /** Whether to display the date before the day of the week. Defaults to true. */
  showDateFirst?: boolean;
  /** The screen width breakpoint in pixels for switching to a small label length (one letter). Defaults to 100. */
  breakpointSmallName?: number;
  /** The screen width breakpoint in pixels for switching to a medium label length (three letters). Defaults to 150. */
  breakpointMediumName?: number;
  /** The screen width breakpoint in pixels for switching to a single column layout. Defaults to 70. */
  breakpointColumn?: number;
  /**
   * Callback used to determine when to use the custom rendering.
   * @param date the date
   * @param weekdayInfo the weekday info
   * @param headerRow the header row
   * @returns true if the custom rendering should be used, false otherwise
   */
  useRenderDay?: (date: string, weekdayInfo: HfWeekdayInfo, headerRow: number) => boolean;
  /**
   * Callback used to render the day.
   * @param date the date
   * @param weekdayInfo the weekday info
   * @param headerRow the header row
   * @returns the custom day inner HTML
   */
  renderDay?: (date: string, weekdayInfo: HfWeekdayInfo, headerRow: number) => string;
}

/**
 * The weekday info model.
 */
export class HfWeekdayInfo {
  /** The name of the weekday. */
  name: string;
  /** The short name of the weekday. */
  shortName: string;
  /** The index of the weekday. */
  narrowName: string;
  /** The default name of the weekday - in English. */
  defaultName: string;
  /** The index of the weekday. Sunday - 0, Monday - 1, ..., Saturday - 6 */
  index: number;
}

/**
 * The header resource configuration.
 */
export class HfHeaderResourceConfig {
  /** Whether to display the image. Defaults to true. */
  showImage?: boolean;
  /** Whether to display the title. Defaults to true. */
  showTitle?: boolean;
  /** Whether to display the color. Defaults to false. */
  showColor?: boolean;
  /** The resource avatar configuration. */
  avatarConfig?: HfResourceAvatarConfig;
  /** The screen width breakpoint in pixels for switching to a single column layout. Defaults to 150. */
  breakpointColumn?: number;
  /**
   * Callback used to determine when to use the custom resource rendering.
   * @param resource the resource
   * @param headerRow the header row
   * @returns true if the custom rendering should be used, false otherwise
   */
  useRenderResource?: (resource: HfResource, headerRow: number) => boolean;
  /**
   * Callback used to render the resource.
   * @param resource the resource
   * @param headerRow the header row
   * @returns the custom resource inner HTML
   */
  renderResource?: (resource: HfResource, headerRow: number) => string;
  /**
   * Callback used to determine when to use the custom resource image rendering.
   * @param resource the resource
   * @param headerRow the header row
   * @returns true if the custom rendering should be used, false otherwise
   */
  useRenderResourceImage?: (resource: HfResource, headerRow: number) => boolean;
  /**
   * Callback used to render the resource image.
   * @param resource the resource
   * @param headerRow the header row
   * @returns the custom resource image inner HTML
   */
  renderResourceImage?: (resource: HfResource, headerRow: number) => string;
}

/**
 * The resource avatar configuration.
 */
export class HfResourceAvatarConfig {
  /** The maximum number of initials to display. Defaults to 3. */
  maxInitials?: number;
}

/**
 * The body configuration.
 */
export class HfBodyConfig {
  /** The time cell width in pixels. Defaults to 50. */
  timeCellWidth?: number;
  /** The cell min width in pixels. Defaults to 50. */
  cellMinWidth?: number;
  /** The cell min height in pixels. Defaults to 40. */
  cellMinHeight?: number;
  /** The cell min allowed min width during zoom in pixels. Defaults to 20. */
  minZoomCellMinWidth?: number;
  /** The cell max allowed min height during zoom in pixels. Defaults to 200. */
  maxZoomCellMinWidth?: number;
  /** The cell min allowed min height during zoom in pixels. Defaults to 20. */
  minZoomCellMinHeight?: number;
  /** The cell max allowed min height during zoom in pixels. Defaults to 200. */
  maxZoomCellMinHeight?: number;
  /** The event min duration in minutes. Defaults to 30. */
  eventMinDurationInMinutes?: number;
  /** The snap duration in minutes. Defaults to 15. */
  snapDurationInMinutes?: number;
  /** The switch drag resize action. Defaults to 'hold'. */
  switchDragResizeAction?: string;
  /** The select action. Defaults to 'tap'. */
  selectAction?: string;
  /** The drag resize states. Default to ['dragResize']. */
  dragResizeStates?: string[];
  /** The mobile drag resize states. Default to ['none', 'dragResize']. */
  mobileDragResizeStates?: string[];
  /** Whether to display the time indicator. Defaults to true. */
  showTimeIndicator?: boolean;
  /** The event configuration. */
  eventConfig?: HfEventConfig;
  /** The event configuration. */
  backgroundEventConfig?: HfEventConfig;
  /** Whether to enable the new events functionality. Defaults to false. */
  enableNewEvents?: boolean;
  /** Whether the events are allowed to overlap with other events. Defaults to true. */
  eventOverlap?: boolean;
  /** The active hours. */
  activeHours?: {
    /** The start time in 'HH:mm format. */
    startTime: string;
    /** The end time in 'HH:mm format. */
    endTime: string;
  };
}

/**
 * The event configuration.
 */
export class HfEventConfig {
  /** Whether to show the title. Defaults to true. */
  showTitle?: boolean;
  /** Whether to show the description. Defaults to false. */
  showDescription?: boolean;
  /** Whether to show the time. Defaults to false. */
  showTime?: boolean;
  /** The resize edges. Defaults to 'both'. */
  resizeEdges?: HfEventResizeEdges;
  /**
   * Callback used to get the event's custom style.
   * @param event the event
   * @param columnResourceId the column resource id
   * @returns the custom event style
   */
  styleSource?: (event: HfEvent, columnResourceId: string) => any;
  /**
   * Callback used to determine when to use the custom rendering.
   * @param event the event
   * @param columnResourceId the column resource id
   * @returns true if the custom rendering should be used, false otherwise
   */
  useRenderEvent?: (event: HfEvent, columnResourceId: string) => boolean;
  /**
   * Callback used to render the event.
   * @param event the event
   * @param columnResourceId the column resource id
   * @returns the custom event inner HTML
   */
  renderEvent?: (event: HfEvent, columnResourceId: string) => string;
}

/**
 * The timegrid event changed event.
 * The event property contains the changed event.
 * The old values that changed are set in the oldXxx properties.
 */
export class HfTimegridEventChangedEvent {
  /** The event that changed. */
  event: HfEvent;
  /** The old resource. */
  oldResource?: HfResource;
  /** The old start time in 'yyyy-MM-dd HH:mm' format. */
  oldStart?: string;
  /** The old end time in 'yyyy-MM-dd HH:mm' format. */
  oldEnd?: string;
}

/**
 * The timegrid event drag resize state changed event.
 * The event property contains the event.
 */
export class HfTimegridEventDragResizeStateChangedEvent {
  /** The event that changed. */
  event: HfEvent;
  /** The new state index. */
  stateIndex: number;
  /** The new state. */
  state: string;
}
```