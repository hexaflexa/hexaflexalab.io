# Plain HTML Example

This is an example of using `hf-timegrid` web component inside a plain HTML file:

```html
<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=5.0" />
    <title>HexaFlexa Timegrid</title>

    <!-- Install Swiper -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-element-bundle.min.js"></script>

    <!-- Install Timegrid -->
    <script type="module" src="https://unpkg.com/@hexaflexa/timegrid@1/dist/timegrid/timegrid.esm.js"></script>
    <script nomodule src="https://unpkg.com/@hexaflexa/timegrid@1/dist/timegrid/timegrid.esm.js"></script>
  </head>
  <body>
    <h1>HexaFlexa Timegrid Demo</h1>

    <hf-timegrid></hf-timegrid>

    <script>
      const startDate = new Date().toISOString().split('T')[0];

      const timegridConfig = {
        daysConfig: {
          daysCount: 3,
        },
        resources: [
          { id: '1', title: 'Resource 1' },
          { id: '2', title: 'Resource 2' },
        ],
        events: [
          {
            id: '1',
            resources: ['1'],
            title: 'Event 1',
            start: `${startDate} 09:00`,
            end: `${startDate} 10:00`,
          },
          {
            id: '2',
            resources: ['2'],
            title: 'Event 2',
            start: `${startDate} 10:00`,
            end: `${startDate} 11:00`,
          },
        ],
      };

      const newEventHandler = function(event) {
        console.log(event.detail);
        alert('New event:' + JSON.stringify(event.detail));
      };

      const timegrid = document.querySelector('hf-timegrid');
      timegrid.startDate = startDate;
      timegrid.config = timegridConfig;
      timegrid.addEventListener('eventNew', newEventHandler);
    </script>

    <style>
      hf-timegrid {
        width: 500px;
        height: 90vh;
        border: 2px solid #ddd;
        border-radius: 10px;
      }

      hf-timegrid::part(body-event) {
        padding: 4px;
        background-color: #99f7f7;
        color: #aa00ff;
        font-size: 1em;
        border-left: 3px solid magenta;
        border-radius: 0;
        border-top-right-radius: 0.5em;
        border-bottom-right-radius: 0.5em;
      }
    </style>
  </body>
</html>
```
