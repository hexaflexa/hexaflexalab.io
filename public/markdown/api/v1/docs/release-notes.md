# Release notes

## 1.0.x (2024-02-15)

- Initial release

## 1.1.0 (2024-03-15)

- Added support for background events

### 1.1.1 (2024-03-15)

- Fix event show/hide title issue

### 1.1.2 (2024-03-16)

- Fix background events interfering with new event

### 1.1.3 (2024-03-29)

- Added support for event overlap restrictions (in general, for resource events, for custom events, for background events)

### 1.1.4 (2024-03-30)

- Added event overlap restrictions during event resize

## 1.2.0 (2024-04-04)

- Added show / hide time indicator flag
- Fix time indicator time to use local time
- Split events into separate lists (or sources) for events and background events

## 1.3.0 (2024-05-08)

- Added refresh event element method for external event updates

### 1.3.1 (2024-05-12)

- Make timegrid scrollable from the time column
- Enable event custom column style source

## 1.4.0 (2024-05-19)

- Improve drag/resize configuration (mark events as not draggable, or not resizeable, set enabled resize edges)

### 1.4.1 (2024-05-26)

- Improve clone drag behavior (refresh time, move pair event elements)
- Improve resize behavior (refresh time, add clone event elements)

## 1.5.0 (2024-06-02)

- Added pinch zoom support for mobile devices

## 1.6.0 (2024-06-10)

- Use *tap* by default to select events, and *hold* to switch between drag and resize states
- On desktop: display resize handles only on hover, use only dragResize state by default
- On mobile: use none + dragResize states by default

### 1.6.1 (2024-06-13)

- Added disableSwiping option

### 1.6.2 (2024-07-08)

- Updated license: Free for Non-Commercial Use

### 1.6.3 (2025-01-12)

- Fix bug for no resource events drag and resize

### 1.6.4 (2025-02-03)

- Improve eventSource and backgroundEventSource usage

### 1.6.5 (2025-02-06)

- Fix bug related to eventSource and backgroundEventSource usage

### 1.6.6 (2025-02-10)

- Fix background events bug