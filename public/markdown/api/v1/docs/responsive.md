# Responsive design

The Timegrid component is a designed to display time-based schedules in a responsive manner. It automatically adjusts its layout and sizing based on the screen width.

# Features

- Responsive design: Automatically adjusts layout and sizing for optimal viewing experience across various devices and screen sizes.
- Customizable breakpoints: Configurable breakpoints determine when to switch between different layouts and label lengths.
- Dynamic resizing: Header date and resource cells resize proportionally with screen width changes or window resizing.
- Horizontal and vertical layouts: Supports both horizontal and vertical orientations for displaying the header cell contents.

# Header date cells

The header date cells are designed to display the date and day of the week. The cells automatically resize based on the screen width and the length of the date and day labels.

The component can be configured using the `HfHeaderDayConfig` class. The following properties are available:

```typescript
/**
 * The header day configuration.
 */
export class HfHeaderDayConfig {
  /* Whether to display the date. Defaults to true. */
  showDate?: boolean;
  /* Whether to display the weekday. Defaults to true. */
  showWeekday?: boolean;
  /* Whether to display the date before the day of the week. Defaults to true. */
  showDateFirst?: boolean;
  /* The screen width breakpoint in pixels for switching to a small label length (one letter). Defaults to 100. */
  breakpointSmallName?: number;
  /* The screen width breakpoint in pixels for switching to a medium label length (three letters). Defaults to 150. */
  breakpointMediumName?: number;
  /* The screen width breakpoint in pixels for switching to a single column layout. Defaults to 70. */
  breakpointColumn?: number;
  /**
   * Callback used to determine when to use the custom rendering.
   * @param date the date
   * @param weekdayInfo the weekday info
   * @param headerRow the header row
   * @returns true if the custom rendering should be used, false otherwise
   */
  useRenderDay?: (date: string, weekdayInfo: HfWeekdayInfo, headerRow: number) => boolean;
  /**
   * Callback used to render the day.
   * @param date the date
   * @param weekdayInfo the weekday info
   * @param headerRow the header row
   * @returns the custom day inner HTML
   */
  renderDay?: (date: string, weekdayInfo: HfWeekdayInfo, headerRow: number) => string;
}

/**
 * The weekday info model.
 */
export class HfWeekdayInfo {
  /* The name of the weekday. */
  name: string;
  /* The short name of the weekday. */
  shortName: string;
  /* The index of the weekday. */
  narrowName: string;
  /* The default name of the weekday - in English. */
  defaultName: string;
  /* The index of the weekday. Sunday - 0, Monday - 1, ..., Saturday - 6 */
  index: number;
}
```

The user can choose to display only the date or the day of the week using the `showDate` and `showWeekday` properties.

The user can also choose to display the date and day of the week in any order using the `showDateFirst` property.

The default layout is in a row. The user can choose from which screen width the layout should switch to a single column layout using the `breakpointColumn` property.

The default date name is the full word in the locale configured by the user (defaults to English: `en`). The user can also choose from which screen width the label length should change to 1 or 3 letters, using the `breakpointSmallName` and `breakpointMediumName` properties.

If these options are not enough, the user can also choose to render the date and day of the week using the `useRenderDay` and `renderDay` properties.
If `renderDay` is not provided, the default date and day of the week will be used.
If `renderDay` is provided, the `useRenderDay` property will be used to determine when to use the custom rendering. If `useRenderDay` is not provided, the custom rendering will be used for all cells.

# Header resource cells

The header resource cells are designed to display the resource avatar and name. The cells automatically resize based on the screen width and the length of the resource name.

The component can be configured using the `HfHeaderResourceConfig` class. The following properties are available:

```typescript
/**
 * The header resource configuration.
 */
export class HfHeaderResourceConfig {
  /* Whether to display the image. Defaults to true. */
  showImage?: boolean;
  /* Whether to display the title. Defaults to true. */
  showTitle?: boolean;
  /* Whether to display the color. Defaults to false. */
  showColor?: boolean;
  /* The resource avatar configuration. */
  avatarConfig?: HfResourceAvatarConfig;
  /* The screen width breakpoint in pixels for switching to a single column layout. Defaults to 150. */
  breakpointColumn?: number;
  /**
   * Callback used to determine when to use the custom resource rendering.
   * @param resource the resource
   * @param headerRow the header row
   * @returns true if the custom rendering should be used, false otherwise
   */
  useRenderResource?: (resource: HfResource, headerRow: number) => boolean;
  /**
   * Callback used to render the resource.
   * @param resource the resource
   * @param headerRow the header row
   * @returns the custom resource inner HTML
   */
  renderResource?: (resource: HfResource, headerRow: number) => string;
  /**
   * Callback used to determine when to use the custom resource image rendering.
   * @param resource the resource
   * @param headerRow the header row
   * @returns true if the custom rendering should be used, false otherwise
   */
  useRenderResourceImage?: (resource: HfResource, headerRow: number) => boolean;
  /**
   * Callback used to render the resource image.
   * @param resource the resource
   * @param headerRow the header row
   * @returns the custom resource image inner HTML
   */
  renderResourceImage?: (resource: HfResource, headerRow: number) => string;
}

/**
 * The resource avatar configuration.
 */
export class HfResourceAvatarConfig {
  /* The maximum number of initials to display. Defaults to 3. */
  maxInitials?: number;
}
```

The user can choose to display the resource avatar, name, and color using the `showImage`, `showTitle`, and `showColor` properties.

The user can configure how to display the resource avatar using the `HfResourceAvatarConfig` class. The user can choose the maximum number of initials to display using the `maxInitials` property.

The default layout is in a row. The user can choose from which screen width the layout should switch to a single column layout using the `breakpointColumn` property.

The user can choose to display the resource avatar, name, and color using the `showImage`, `showTitle`, and `showColor` properties.

If these options are not enough, the user can also choose to render the resource using the `useRenderResource` and `renderResource` properties.
If `renderResource` is not provided, the default resource avatar and name will be used.
If `renderResource` is provided, the `useRenderResource` property will be used to determine when to use the custom rendering. If `useRenderResource` is not provided, the custom rendering will be used for all cells.

