# Styling information

The `hf-timegrid` web component uses shadow DOM, so the styles are encapsulated.

The other web components that are used internally by the `hf-timegrid` web component also use shadow DOM.

The web components expose shadow parts, which can be used to style the components using CSS.

They also export the shadow parts of the web components that are used internally.

Some web components also expose CSS variables, which can be used to style the components.

An example of how to style the `hf-timegrid` web component using the parts and the CSS variables is shown below:

```css
hf-timegrid::part(header) {
  background-color: #fef;

  border-left: 2px dashed gray;
  border-right: 2px dashed gray;
  border-top: 2px dashed gray;
 
  --bottom-border: 2px solid magenta;
  --time-grid-vertical-border: 2px dashed magenta;
  --grid-horizontal-border: 1px solid red;
  --grid-vertical-border: 1px solid red;
  --grid-vertical-group-border: 2px solid magenta;
  --grid-vertical-right-border: 1px solid #ccc;
}
```

## Shadow parts

```mermaid
classDiagram
  hf-timegrid o--> hf-timegrid-toolbar : export parts
  hf-timegrid o--> hf-timegrid-slide : export parts
  hf-timegrid-slide o--> hf-timegrid-header : export parts
  hf-timegrid-header o--> hf-timegrid-header-date : export parts
  hf-timegrid-header o--> hf-timegrid-header-resource : export parts
  hf-timegrid-slide o--> hf-timegrid-body : export parts
  hf-timegrid-body o--> hf-timegrid-body-event : export parts

  class hf-timegrid {
    toolbar
    slide
  }
  class hf-timegrid-toolbar {
    toolbar-button
    toolbar-start
    toolbar-center
    toolbar-end
    toolbar-loading
  }
  class hf-timegrid-slide {
    header
    body
  }
  class hf-timegrid-header {
    header-date
    header-resource
  }
  class hf-timegrid-header-date {
    header-date-day
    header-date-day-0
    header-date-day-1
    header-date-day-2
    header-date-day-3
    header-date-day-4
    header-date-day-5
    header-date-day-6
    header-date-day-today
    header-date-name
    header-date-name-0
    header-date-name-1
    header-date-name-2
    header-date-name-3
    header-date-name-4
    header-date-name-5
    header-date-name-6
    header-date-name-today
  }
  class hf-timegrid-header-resource {
    header-resource-image
    header-resource-avatar
    header-resource-title
  }
  class hf-timegrid-body {
    body-time
    body-time-label
    body-grid
    body-event
    body-event-new
  }
  class hf-timegrid-body-event {
    body-event-content
    body-event-title
    body-event-description
    body-event-time
    body-event-resize-top
    body-event-resize-bottom
  }
```

## CSS variables

```mermaid
classDiagram
  hf-timegrid o--> hf-timegrid-toolbar
  hf-timegrid o--> hf-timegrid-slide
  hf-timegrid-slide o--> hf-timegrid-header
  hf-timegrid-header o--> hf-timegrid-header-date
  hf-timegrid-header o--> hf-timegrid-header-resource
  hf-timegrid-slide o--> hf-timegrid-body
  hf-timegrid-body o--> hf-timegrid-body-event

  class hf-timegrid-header {
    --time-cell-background: none
    --scrollbar-background: none
    --bottom-border: 2px solid #bbbbbb
    --time-grid-vertical-border: 2px solid transparent
    --grid-horizontal-border: none;
    --grid-vertical-border: 1px solid #bbbbbb
    --grid-vertical-group-border: 2px solid #bbbbbb
    --grid-vertical-right-border: 1px solid #ccc
  }
  class hf-timegrid-header-resource {
    --background-color: #326496
    --colorbar-height: 3px
  }
  class hf-timegrid-body {
    --time-background: #ffffff
    --time-indicator-color: #3880ff
    --time-indicator-background: #ffffff66
    --time-label-color: #92949c
    --grid-background: white;
    --grid-dropzone-active-background: #eeffffcc
    --grid-dropzone-enter-background: #ddffffcc
    --grid-dropzone-active-reject-background: #ffeeffcc
    --grid-dropzone-enter-reject-background: #ffddffcc
    --time-horizontal-border: 1px solid transparent
    --time-grid-vertical-border: 2px solid transparent
    --grid-horizontal-border: 1px solid #edeef0
    --grid-vertical-border: 1px solid #edeef0
    --grid-vertical-group-border: 2px solid #edeef0
  }
  class hf-timegrid-body-event {
    --background: #29e
    --color: white
    --border: none
    --border-radius: 0.5em
    --draggable-background: #17c
    --draggable-color: --color
    --draggable-border: --border
    --resizeable-background: --background
    --resizeable-color: --color
    --resizeable-border: --border
    --draggable-resizeable-background: --draggable-background
    --draggable-resizeable-color: --color
    --draggable-resizeable-border: --border
    --resize-background: --color
    --resize-border: 1px solid --background
    --resize-border-radius: 50%
    --new-background: #17ca
    --new-color: --color
    --new-border: --border
  }
```
