[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfBodyConfig

# Class: HfBodyConfig

[timegrid-model](../modules/timegrid_model.md).HfBodyConfig

The body configuration.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfBodyConfig.md#constructor)

### Properties

- [activeHours](timegrid_model.HfBodyConfig.md#activehours)
- [backgroundEventConfig](timegrid_model.HfBodyConfig.md#backgroundeventconfig)
- [cellMinHeight](timegrid_model.HfBodyConfig.md#cellminheight)
- [cellMinWidth](timegrid_model.HfBodyConfig.md#cellminwidth)
- [dragResizeStates](timegrid_model.HfBodyConfig.md#dragresizestates)
- [enableNewEvents](timegrid_model.HfBodyConfig.md#enablenewevents)
- [eventConfig](timegrid_model.HfBodyConfig.md#eventconfig)
- [eventMinDurationInMinutes](timegrid_model.HfBodyConfig.md#eventmindurationinminutes)
- [eventOverlap](timegrid_model.HfBodyConfig.md#eventoverlap)
- [maxZoomCellMinHeight](timegrid_model.HfBodyConfig.md#maxzoomcellminheight)
- [maxZoomCellMinWidth](timegrid_model.HfBodyConfig.md#maxzoomcellminwidth)
- [minZoomCellMinHeight](timegrid_model.HfBodyConfig.md#minzoomcellminheight)
- [minZoomCellMinWidth](timegrid_model.HfBodyConfig.md#minzoomcellminwidth)
- [mobileDragResizeStates](timegrid_model.HfBodyConfig.md#mobiledragresizestates)
- [selectAction](timegrid_model.HfBodyConfig.md#selectaction)
- [showTimeIndicator](timegrid_model.HfBodyConfig.md#showtimeindicator)
- [snapDurationInMinutes](timegrid_model.HfBodyConfig.md#snapdurationinminutes)
- [switchDragResizeAction](timegrid_model.HfBodyConfig.md#switchdragresizeaction)
- [timeCellWidth](timegrid_model.HfBodyConfig.md#timecellwidth)

## Constructors

### constructor

• **new HfBodyConfig**(): [`HfBodyConfig`](timegrid_model.HfBodyConfig.md)

#### Returns

[`HfBodyConfig`](timegrid_model.HfBodyConfig.md)

## Properties

### activeHours

• `Optional` **activeHours**: `Object`

The active hours.

#### Type declaration

| Name | Type | Description |
| :------ | :------ | :------ |
| `endTime` | `string` | The end time in 'HH:mm format. |
| `startTime` | `string` | The start time in 'HH:mm format. |

___

### backgroundEventConfig

• `Optional` **backgroundEventConfig**: [`HfEventConfig`](timegrid_model.HfEventConfig.md)

The event configuration.

___

### cellMinHeight

• `Optional` **cellMinHeight**: `number`

The cell min height in pixels. Defaults to 40.

___

### cellMinWidth

• `Optional` **cellMinWidth**: `number`

The cell min width in pixels. Defaults to 50.

___

### dragResizeStates

• `Optional` **dragResizeStates**: `string`[]

The drag resize states. Default to ['dragResize'].

___

### enableNewEvents

• `Optional` **enableNewEvents**: `boolean`

Whether to enable the new events functionality. Defaults to false.

___

### eventConfig

• `Optional` **eventConfig**: [`HfEventConfig`](timegrid_model.HfEventConfig.md)

The event configuration.

___

### eventMinDurationInMinutes

• `Optional` **eventMinDurationInMinutes**: `number`

The event min duration in minutes. Defaults to 30.

___

### eventOverlap

• `Optional` **eventOverlap**: `boolean`

Whether the events are allowed to overlap with other events. Defaults to true.

___

### maxZoomCellMinHeight

• `Optional` **maxZoomCellMinHeight**: `number`

The cell max allowed min height during zoom in pixels. Defaults to 200.

___

### maxZoomCellMinWidth

• `Optional` **maxZoomCellMinWidth**: `number`

The cell max allowed min height during zoom in pixels. Defaults to 200.

___

### minZoomCellMinHeight

• `Optional` **minZoomCellMinHeight**: `number`

The cell min allowed min height during zoom in pixels. Defaults to 20.

___

### minZoomCellMinWidth

• `Optional` **minZoomCellMinWidth**: `number`

The cell min allowed min width during zoom in pixels. Defaults to 20.

___

### mobileDragResizeStates

• `Optional` **mobileDragResizeStates**: `string`[]

The mobile drag resize states. Default to ['none', 'dragResize'].

___

### selectAction

• `Optional` **selectAction**: `string`

The select action. Defaults to 'tap'.

___

### showTimeIndicator

• `Optional` **showTimeIndicator**: `boolean`

Whether to display the time indicator. Defaults to true.

___

### snapDurationInMinutes

• `Optional` **snapDurationInMinutes**: `number`

The snap duration in minutes. Defaults to 15.

___

### switchDragResizeAction

• `Optional` **switchDragResizeAction**: `string`

The switch drag resize action. Defaults to 'hold'.

___

### timeCellWidth

• `Optional` **timeCellWidth**: `number`

The time cell width in pixels. Defaults to 50.
