[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfDaysConfig

# Class: HfDaysConfig

[timegrid-model](../modules/timegrid_model.md).HfDaysConfig

The days configuration.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfDaysConfig.md#constructor)

### Properties

- [activeDayChecker](timegrid_model.HfDaysConfig.md#activedaychecker)
- [daysCount](timegrid_model.HfDaysConfig.md#dayscount)
- [fullWeek](timegrid_model.HfDaysConfig.md#fullweek)
- [includeInactiveDays](timegrid_model.HfDaysConfig.md#includeinactivedays)

## Constructors

### constructor

• **new HfDaysConfig**(): [`HfDaysConfig`](timegrid_model.HfDaysConfig.md)

#### Returns

[`HfDaysConfig`](timegrid_model.HfDaysConfig.md)

## Properties

### activeDayChecker

• `Optional` **activeDayChecker**: (`date`: `string`) => `boolean`

The active day checker.

**`Param`**

the date string in format yyyy-MM-dd

#### Type declaration

▸ (`date`): `boolean`

The active day checker.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `string` | the date string in format yyyy-MM-dd |

##### Returns

`boolean`

true if the day is active, false otherwise

___

### daysCount

• `Optional` **daysCount**: `number`

The number of days to display. Defaults to 1.

___

### fullWeek

• `Optional` **fullWeek**: `boolean`

Whether to display the full week. Defaults to false.

___

### includeInactiveDays

• `Optional` **includeInactiveDays**: `boolean`

Whether to include the inactive days. Defaults to true.
