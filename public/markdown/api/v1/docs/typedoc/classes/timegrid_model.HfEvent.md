[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfEvent

# Class: HfEvent

[timegrid-model](../modules/timegrid_model.md).HfEvent

The event model.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfEvent.md#constructor)

### Properties

- [description](timegrid_model.HfEvent.md#description)
- [display](timegrid_model.HfEvent.md#display)
- [dragResize](timegrid_model.HfEvent.md#dragresize)
- [end](timegrid_model.HfEvent.md#end)
- [extendedProps](timegrid_model.HfEvent.md#extendedprops)
- [groupId](timegrid_model.HfEvent.md#groupid)
- [id](timegrid_model.HfEvent.md#id)
- [overlap](timegrid_model.HfEvent.md#overlap)
- [resources](timegrid_model.HfEvent.md#resources)
- [start](timegrid_model.HfEvent.md#start)
- [style](timegrid_model.HfEvent.md#style)
- [title](timegrid_model.HfEvent.md#title)

## Constructors

### constructor

• **new HfEvent**(): [`HfEvent`](timegrid_model.HfEvent.md)

#### Returns

[`HfEvent`](timegrid_model.HfEvent.md)

## Properties

### description

• `Optional` **description**: `string`

The event description.

___

### display

• `Optional` **display**: [`HfEventDisplay`](../modules/timegrid_model.md#hfeventdisplay)

The event display. Possible values: 'default', 'none', 'background'

___

### dragResize

• `Optional` **dragResize**: [`HfEventDragResize`](timegrid_model.HfEventDragResize.md)

The event interact configuration.

___

### end

• **end**: `string`

The end date and time in 'yyyy-MM-dd HH:mm' format.

___

### extendedProps

• `Optional` **extendedProps**: `any`

The event extended properties.

___

### groupId

• `Optional` **groupId**: `string`

The group id.

___

### id

• **id**: `string`

The event id.

___

### overlap

• `Optional` **overlap**: `boolean`

Whether the event is allowed to overlap with other events. Defaults to true.

___

### resources

• `Optional` **resources**: `string`[]

The array of resource ids the event is associated with.

___

### start

• **start**: `string`

The start date and time in 'yyyy-MM-dd HH:mm' format.

___

### style

• `Optional` **style**: `any`

The event custom style.

___

### title

• `Optional` **title**: `string`

The event title.
