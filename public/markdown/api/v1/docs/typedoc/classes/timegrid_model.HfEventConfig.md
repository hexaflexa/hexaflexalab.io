[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfEventConfig

# Class: HfEventConfig

[timegrid-model](../modules/timegrid_model.md).HfEventConfig

The event configuration.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfEventConfig.md#constructor)

### Properties

- [renderEvent](timegrid_model.HfEventConfig.md#renderevent)
- [resizeEdges](timegrid_model.HfEventConfig.md#resizeedges)
- [showDescription](timegrid_model.HfEventConfig.md#showdescription)
- [showTime](timegrid_model.HfEventConfig.md#showtime)
- [showTitle](timegrid_model.HfEventConfig.md#showtitle)
- [styleSource](timegrid_model.HfEventConfig.md#stylesource)
- [useRenderEvent](timegrid_model.HfEventConfig.md#userenderevent)

## Constructors

### constructor

• **new HfEventConfig**(): [`HfEventConfig`](timegrid_model.HfEventConfig.md)

#### Returns

[`HfEventConfig`](timegrid_model.HfEventConfig.md)

## Properties

### renderEvent

• `Optional` **renderEvent**: (`event`: [`HfEvent`](timegrid_model.HfEvent.md), `columnResourceId`: `string`) => `string`

Callback used to render the event.

**`Param`**

the event

**`Param`**

the column resource id

#### Type declaration

▸ (`event`, `columnResourceId`): `string`

Callback used to render the event.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `event` | [`HfEvent`](timegrid_model.HfEvent.md) | the event |
| `columnResourceId` | `string` | the column resource id |

##### Returns

`string`

the custom event inner HTML

___

### resizeEdges

• `Optional` **resizeEdges**: [`HfEventResizeEdges`](../modules/timegrid_model.md#hfeventresizeedges)

The resize edges. Defaults to 'both'.

___

### showDescription

• `Optional` **showDescription**: `boolean`

Whether to show the description. Defaults to false.

___

### showTime

• `Optional` **showTime**: `boolean`

Whether to show the time. Defaults to false.

___

### showTitle

• `Optional` **showTitle**: `boolean`

Whether to show the title. Defaults to true.

___

### styleSource

• `Optional` **styleSource**: (`event`: [`HfEvent`](timegrid_model.HfEvent.md), `columnResourceId`: `string`) => `any`

Callback used to get the event's custom style.

**`Param`**

the event

**`Param`**

the column resource id

#### Type declaration

▸ (`event`, `columnResourceId`): `any`

Callback used to get the event's custom style.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `event` | [`HfEvent`](timegrid_model.HfEvent.md) | the event |
| `columnResourceId` | `string` | the column resource id |

##### Returns

`any`

the custom event style

___

### useRenderEvent

• `Optional` **useRenderEvent**: (`event`: [`HfEvent`](timegrid_model.HfEvent.md), `columnResourceId`: `string`) => `boolean`

Callback used to determine when to use the custom rendering.

**`Param`**

the event

**`Param`**

the column resource id

#### Type declaration

▸ (`event`, `columnResourceId`): `boolean`

Callback used to determine when to use the custom rendering.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `event` | [`HfEvent`](timegrid_model.HfEvent.md) | the event |
| `columnResourceId` | `string` | the column resource id |

##### Returns

`boolean`

true if the custom rendering should be used, false otherwise
