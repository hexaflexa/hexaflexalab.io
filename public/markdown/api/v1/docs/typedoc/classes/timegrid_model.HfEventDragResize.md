[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfEventDragResize

# Class: HfEventDragResize

[timegrid-model](../modules/timegrid_model.md).HfEventDragResize

The event interact type.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfEventDragResize.md#constructor)

### Properties

- [draggable](timegrid_model.HfEventDragResize.md#draggable)
- [resizable](timegrid_model.HfEventDragResize.md#resizable)
- [resizeEdges](timegrid_model.HfEventDragResize.md#resizeedges)

## Constructors

### constructor

• **new HfEventDragResize**(): [`HfEventDragResize`](timegrid_model.HfEventDragResize.md)

#### Returns

[`HfEventDragResize`](timegrid_model.HfEventDragResize.md)

## Properties

### draggable

• `Optional` **draggable**: `boolean`

Whether the event is draggable. Defaults to true.

___

### resizable

• `Optional` **resizable**: `boolean`

Whether the event is resizable. Defaults to true.

___

### resizeEdges

• `Optional` **resizeEdges**: [`HfEventResizeEdges`](../modules/timegrid_model.md#hfeventresizeedges)

The resize edges. Defaults to 'both'.
