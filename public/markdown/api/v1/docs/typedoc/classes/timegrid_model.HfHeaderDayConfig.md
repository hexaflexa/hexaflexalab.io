[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfHeaderDayConfig

# Class: HfHeaderDayConfig

[timegrid-model](../modules/timegrid_model.md).HfHeaderDayConfig

The header day configuration.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfHeaderDayConfig.md#constructor)

### Properties

- [breakpointColumn](timegrid_model.HfHeaderDayConfig.md#breakpointcolumn)
- [breakpointMediumName](timegrid_model.HfHeaderDayConfig.md#breakpointmediumname)
- [breakpointSmallName](timegrid_model.HfHeaderDayConfig.md#breakpointsmallname)
- [renderDay](timegrid_model.HfHeaderDayConfig.md#renderday)
- [showDate](timegrid_model.HfHeaderDayConfig.md#showdate)
- [showDateFirst](timegrid_model.HfHeaderDayConfig.md#showdatefirst)
- [showWeekday](timegrid_model.HfHeaderDayConfig.md#showweekday)
- [useRenderDay](timegrid_model.HfHeaderDayConfig.md#userenderday)

## Constructors

### constructor

• **new HfHeaderDayConfig**(): [`HfHeaderDayConfig`](timegrid_model.HfHeaderDayConfig.md)

#### Returns

[`HfHeaderDayConfig`](timegrid_model.HfHeaderDayConfig.md)

## Properties

### breakpointColumn

• `Optional` **breakpointColumn**: `number`

The screen width breakpoint in pixels for switching to a single column layout. Defaults to 70.

___

### breakpointMediumName

• `Optional` **breakpointMediumName**: `number`

The screen width breakpoint in pixels for switching to a medium label length (three letters). Defaults to 150.

___

### breakpointSmallName

• `Optional` **breakpointSmallName**: `number`

The screen width breakpoint in pixels for switching to a small label length (one letter). Defaults to 100.

___

### renderDay

• `Optional` **renderDay**: (`date`: `string`, `weekdayInfo`: [`HfWeekdayInfo`](timegrid_model.HfWeekdayInfo.md), `headerRow`: `number`) => `string`

Callback used to render the day.

**`Param`**

the date

**`Param`**

the weekday info

**`Param`**

the header row

#### Type declaration

▸ (`date`, `weekdayInfo`, `headerRow`): `string`

Callback used to render the day.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `string` | the date |
| `weekdayInfo` | [`HfWeekdayInfo`](timegrid_model.HfWeekdayInfo.md) | the weekday info |
| `headerRow` | `number` | the header row |

##### Returns

`string`

the custom day inner HTML

___

### showDate

• `Optional` **showDate**: `boolean`

Whether to display the date. Defaults to true.

___

### showDateFirst

• `Optional` **showDateFirst**: `boolean`

Whether to display the date before the day of the week. Defaults to true.

___

### showWeekday

• `Optional` **showWeekday**: `boolean`

Whether to display the weekday. Defaults to true.

___

### useRenderDay

• `Optional` **useRenderDay**: (`date`: `string`, `weekdayInfo`: [`HfWeekdayInfo`](timegrid_model.HfWeekdayInfo.md), `headerRow`: `number`) => `boolean`

Callback used to determine when to use the custom rendering.

**`Param`**

the date

**`Param`**

the weekday info

**`Param`**

the header row

#### Type declaration

▸ (`date`, `weekdayInfo`, `headerRow`): `boolean`

Callback used to determine when to use the custom rendering.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `string` | the date |
| `weekdayInfo` | [`HfWeekdayInfo`](timegrid_model.HfWeekdayInfo.md) | the weekday info |
| `headerRow` | `number` | the header row |

##### Returns

`boolean`

true if the custom rendering should be used, false otherwise
