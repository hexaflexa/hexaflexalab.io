[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfHeaderResourceConfig

# Class: HfHeaderResourceConfig

[timegrid-model](../modules/timegrid_model.md).HfHeaderResourceConfig

The header resource configuration.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfHeaderResourceConfig.md#constructor)

### Properties

- [avatarConfig](timegrid_model.HfHeaderResourceConfig.md#avatarconfig)
- [breakpointColumn](timegrid_model.HfHeaderResourceConfig.md#breakpointcolumn)
- [renderResource](timegrid_model.HfHeaderResourceConfig.md#renderresource)
- [renderResourceImage](timegrid_model.HfHeaderResourceConfig.md#renderresourceimage)
- [showColor](timegrid_model.HfHeaderResourceConfig.md#showcolor)
- [showImage](timegrid_model.HfHeaderResourceConfig.md#showimage)
- [showTitle](timegrid_model.HfHeaderResourceConfig.md#showtitle)
- [useRenderResource](timegrid_model.HfHeaderResourceConfig.md#userenderresource)
- [useRenderResourceImage](timegrid_model.HfHeaderResourceConfig.md#userenderresourceimage)

## Constructors

### constructor

• **new HfHeaderResourceConfig**(): [`HfHeaderResourceConfig`](timegrid_model.HfHeaderResourceConfig.md)

#### Returns

[`HfHeaderResourceConfig`](timegrid_model.HfHeaderResourceConfig.md)

## Properties

### avatarConfig

• `Optional` **avatarConfig**: [`HfResourceAvatarConfig`](timegrid_model.HfResourceAvatarConfig.md)

The resource avatar configuration.

___

### breakpointColumn

• `Optional` **breakpointColumn**: `number`

The screen width breakpoint in pixels for switching to a single column layout. Defaults to 150.

___

### renderResource

• `Optional` **renderResource**: (`resource`: [`HfResource`](timegrid_model.HfResource.md), `headerRow`: `number`) => `string`

Callback used to render the resource.

**`Param`**

the resource

**`Param`**

the header row

#### Type declaration

▸ (`resource`, `headerRow`): `string`

Callback used to render the resource.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `resource` | [`HfResource`](timegrid_model.HfResource.md) | the resource |
| `headerRow` | `number` | the header row |

##### Returns

`string`

the custom resource inner HTML

___

### renderResourceImage

• `Optional` **renderResourceImage**: (`resource`: [`HfResource`](timegrid_model.HfResource.md), `headerRow`: `number`) => `string`

Callback used to render the resource image.

**`Param`**

the resource

**`Param`**

the header row

#### Type declaration

▸ (`resource`, `headerRow`): `string`

Callback used to render the resource image.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `resource` | [`HfResource`](timegrid_model.HfResource.md) | the resource |
| `headerRow` | `number` | the header row |

##### Returns

`string`

the custom resource image inner HTML

___

### showColor

• `Optional` **showColor**: `boolean`

Whether to display the color. Defaults to false.

___

### showImage

• `Optional` **showImage**: `boolean`

Whether to display the image. Defaults to true.

___

### showTitle

• `Optional` **showTitle**: `boolean`

Whether to display the title. Defaults to true.

___

### useRenderResource

• `Optional` **useRenderResource**: (`resource`: [`HfResource`](timegrid_model.HfResource.md), `headerRow`: `number`) => `boolean`

Callback used to determine when to use the custom resource rendering.

**`Param`**

the resource

**`Param`**

the header row

#### Type declaration

▸ (`resource`, `headerRow`): `boolean`

Callback used to determine when to use the custom resource rendering.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `resource` | [`HfResource`](timegrid_model.HfResource.md) | the resource |
| `headerRow` | `number` | the header row |

##### Returns

`boolean`

true if the custom rendering should be used, false otherwise

___

### useRenderResourceImage

• `Optional` **useRenderResourceImage**: (`resource`: [`HfResource`](timegrid_model.HfResource.md), `headerRow`: `number`) => `boolean`

Callback used to determine when to use the custom resource image rendering.

**`Param`**

the resource

**`Param`**

the header row

#### Type declaration

▸ (`resource`, `headerRow`): `boolean`

Callback used to determine when to use the custom resource image rendering.

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `resource` | [`HfResource`](timegrid_model.HfResource.md) | the resource |
| `headerRow` | `number` | the header row |

##### Returns

`boolean`

true if the custom rendering should be used, false otherwise
