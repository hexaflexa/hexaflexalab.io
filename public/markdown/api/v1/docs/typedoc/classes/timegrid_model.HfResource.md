[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfResource

# Class: HfResource

[timegrid-model](../modules/timegrid_model.md).HfResource

The resource model.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfResource.md#constructor)

### Properties

- [backgroundColor](timegrid_model.HfResource.md#backgroundcolor)
- [eventOverlap](timegrid_model.HfResource.md#eventoverlap)
- [extendedProps](timegrid_model.HfResource.md#extendedprops)
- [id](timegrid_model.HfResource.md#id)
- [imageSrc](timegrid_model.HfResource.md#imagesrc)
- [title](timegrid_model.HfResource.md#title)

## Constructors

### constructor

• **new HfResource**(): [`HfResource`](timegrid_model.HfResource.md)

#### Returns

[`HfResource`](timegrid_model.HfResource.md)

## Properties

### backgroundColor

• `Optional` **backgroundColor**: `string`

The resource color.

___

### eventOverlap

• `Optional` **eventOverlap**: `boolean`

Whether the resource events are allowed to overlap with other events. Defaults to true.

___

### extendedProps

• `Optional` **extendedProps**: `any`

The resource extended properties.

___

### id

• **id**: `string`

The resource id.

___

### imageSrc

• `Optional` **imageSrc**: `string`

The resource image source.

___

### title

• **title**: `string`

The resource title.
