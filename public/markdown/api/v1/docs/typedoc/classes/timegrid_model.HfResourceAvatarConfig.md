[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfResourceAvatarConfig

# Class: HfResourceAvatarConfig

[timegrid-model](../modules/timegrid_model.md).HfResourceAvatarConfig

The resource avatar configuration.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfResourceAvatarConfig.md#constructor)

### Properties

- [maxInitials](timegrid_model.HfResourceAvatarConfig.md#maxinitials)

## Constructors

### constructor

• **new HfResourceAvatarConfig**(): [`HfResourceAvatarConfig`](timegrid_model.HfResourceAvatarConfig.md)

#### Returns

[`HfResourceAvatarConfig`](timegrid_model.HfResourceAvatarConfig.md)

## Properties

### maxInitials

• `Optional` **maxInitials**: `number`

The maximum number of initials to display. Defaults to 3.
