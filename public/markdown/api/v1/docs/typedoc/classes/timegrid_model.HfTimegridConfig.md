[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfTimegridConfig

# Class: HfTimegridConfig

[timegrid-model](../modules/timegrid_model.md).HfTimegridConfig

The timegrid configuration model.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfTimegridConfig.md#constructor)

### Properties

- [backgroundEventSource](timegrid_model.HfTimegridConfig.md#backgroundeventsource)
- [backgroundEvents](timegrid_model.HfTimegridConfig.md#backgroundevents)
- [bodyConfig](timegrid_model.HfTimegridConfig.md#bodyconfig)
- [daysConfig](timegrid_model.HfTimegridConfig.md#daysconfig)
- [disableSwiping](timegrid_model.HfTimegridConfig.md#disableswiping)
- [eventSource](timegrid_model.HfTimegridConfig.md#eventsource)
- [events](timegrid_model.HfTimegridConfig.md#events)
- [firstDayOfWeek](timegrid_model.HfTimegridConfig.md#firstdayofweek)
- [groupByResources](timegrid_model.HfTimegridConfig.md#groupbyresources)
- [headerDayConfig](timegrid_model.HfTimegridConfig.md#headerdayconfig)
- [headerResourceConfig](timegrid_model.HfTimegridConfig.md#headerresourceconfig)
- [locale](timegrid_model.HfTimegridConfig.md#locale)
- [resources](timegrid_model.HfTimegridConfig.md#resources)
- [showPoweredBy](timegrid_model.HfTimegridConfig.md#showpoweredby)
- [showToolbar](timegrid_model.HfTimegridConfig.md#showtoolbar)
- [timeFormat](timegrid_model.HfTimegridConfig.md#timeformat)
- [toolbarConfig](timegrid_model.HfTimegridConfig.md#toolbarconfig)

## Constructors

### constructor

• **new HfTimegridConfig**(): [`HfTimegridConfig`](timegrid_model.HfTimegridConfig.md)

#### Returns

[`HfTimegridConfig`](timegrid_model.HfTimegridConfig.md)

## Properties

### backgroundEventSource

• `Optional` **backgroundEventSource**: [`HfEventSource`](../modules/timegrid_model.md#hfeventsource)

The source of background events. If provided, it takes precedence over the background events array.

___

### backgroundEvents

• `Optional` **backgroundEvents**: [`HfEvent`](timegrid_model.HfEvent.md)[]

The background events.

___

### bodyConfig

• `Optional` **bodyConfig**: [`HfBodyConfig`](timegrid_model.HfBodyConfig.md)

The timegrid grid configuration.

___

### daysConfig

• `Optional` **daysConfig**: [`HfDaysConfig`](timegrid_model.HfDaysConfig.md)

The days configuration.

___

### disableSwiping

• `Optional` **disableSwiping**: `boolean`

The disable swipe flag. Defaults to false.

___

### eventSource

• `Optional` **eventSource**: [`HfEventSource`](../modules/timegrid_model.md#hfeventsource)

The source of events. If provided, it takes precedence over the events array.

___

### events

• `Optional` **events**: [`HfEvent`](timegrid_model.HfEvent.md)[]

The events.

___

### firstDayOfWeek

• `Optional` **firstDayOfWeek**: `number`

The index of the first day of the week. Defaults to Sunday - 0.

___

### groupByResources

• `Optional` **groupByResources**: `boolean`

Whether to group the events by resources. Defaults to false.

___

### headerDayConfig

• `Optional` **headerDayConfig**: [`HfHeaderDayConfig`](timegrid_model.HfHeaderDayConfig.md)

The header day configuration.

___

### headerResourceConfig

• `Optional` **headerResourceConfig**: [`HfHeaderResourceConfig`](timegrid_model.HfHeaderResourceConfig.md)

The header resource configuration.

___

### locale

• `Optional` **locale**: `string`

The locale. Used to display the month names and weekday names.

___

### resources

• `Optional` **resources**: [`HfResource`](timegrid_model.HfResource.md)[]

The resources.

___

### showPoweredBy

• `Optional` **showPoweredBy**: `boolean`

Whether to show the Powered by mention. Default to true.

___

### showToolbar

• `Optional` **showToolbar**: `boolean`

Whether to display the toolbar. Defaults to true.

___

### timeFormat

• `Optional` **timeFormat**: `string`

The time format. Valid values: 'HH:mm' and 'hh:mm a'. Defaults to 'HH:mm'.

___

### toolbarConfig

• `Optional` **toolbarConfig**: [`HfToolbarConfig`](timegrid_model.HfToolbarConfig.md)

The toolbar configuration.
