[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfTimegridEventChangedEvent

# Class: HfTimegridEventChangedEvent

[timegrid-model](../modules/timegrid_model.md).HfTimegridEventChangedEvent

The timegrid event changed event.
The event property contains the changed event.
The old values that changed are set in the oldXxx properties.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfTimegridEventChangedEvent.md#constructor)

### Properties

- [event](timegrid_model.HfTimegridEventChangedEvent.md#event)
- [oldEnd](timegrid_model.HfTimegridEventChangedEvent.md#oldend)
- [oldResource](timegrid_model.HfTimegridEventChangedEvent.md#oldresource)
- [oldStart](timegrid_model.HfTimegridEventChangedEvent.md#oldstart)

## Constructors

### constructor

• **new HfTimegridEventChangedEvent**(): [`HfTimegridEventChangedEvent`](timegrid_model.HfTimegridEventChangedEvent.md)

#### Returns

[`HfTimegridEventChangedEvent`](timegrid_model.HfTimegridEventChangedEvent.md)

## Properties

### event

• **event**: [`HfEvent`](timegrid_model.HfEvent.md)

The event that changed.

___

### oldEnd

• `Optional` **oldEnd**: `string`

The old end time in 'yyyy-MM-dd HH:mm' format.

___

### oldResource

• `Optional` **oldResource**: [`HfResource`](timegrid_model.HfResource.md)

The old resource.

___

### oldStart

• `Optional` **oldStart**: `string`

The old start time in 'yyyy-MM-dd HH:mm' format.
