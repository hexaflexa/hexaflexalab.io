[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfTimegridEventDragResizeStateChangedEvent

# Class: HfTimegridEventDragResizeStateChangedEvent

[timegrid-model](../modules/timegrid_model.md).HfTimegridEventDragResizeStateChangedEvent

The timegrid event drag resize state changed event.
The event property contains the event.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfTimegridEventDragResizeStateChangedEvent.md#constructor)

### Properties

- [event](timegrid_model.HfTimegridEventDragResizeStateChangedEvent.md#event)
- [state](timegrid_model.HfTimegridEventDragResizeStateChangedEvent.md#state)
- [stateIndex](timegrid_model.HfTimegridEventDragResizeStateChangedEvent.md#stateindex)

## Constructors

### constructor

• **new HfTimegridEventDragResizeStateChangedEvent**(): [`HfTimegridEventDragResizeStateChangedEvent`](timegrid_model.HfTimegridEventDragResizeStateChangedEvent.md)

#### Returns

[`HfTimegridEventDragResizeStateChangedEvent`](timegrid_model.HfTimegridEventDragResizeStateChangedEvent.md)

## Properties

### event

• **event**: [`HfEvent`](timegrid_model.HfEvent.md)

The event that changed.

___

### state

• **state**: `string`

The new state.

___

### stateIndex

• **stateIndex**: `number`

The new state index.
