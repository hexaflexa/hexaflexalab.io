[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfToolbarConfig

# Class: HfToolbarConfig

[timegrid-model](../modules/timegrid_model.md).HfToolbarConfig

The toolbar configuration.
The accepted controls are: 'prev', 'next', 'today', 'date', 'loading'.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfToolbarConfig.md#constructor)

### Properties

- [centerControls](timegrid_model.HfToolbarConfig.md#centercontrols)
- [disableDateCalendar](timegrid_model.HfToolbarConfig.md#disabledatecalendar)
- [endControls](timegrid_model.HfToolbarConfig.md#endcontrols)
- [startControls](timegrid_model.HfToolbarConfig.md#startcontrols)

## Constructors

### constructor

• **new HfToolbarConfig**(): [`HfToolbarConfig`](timegrid_model.HfToolbarConfig.md)

#### Returns

[`HfToolbarConfig`](timegrid_model.HfToolbarConfig.md)

## Properties

### centerControls

• `Optional` **centerControls**: [`HfToolbarControl`](../modules/timegrid_model.md#hftoolbarcontrol)[]

The controls to be displayed at the center.

___

### disableDateCalendar

• `Optional` **disableDateCalendar**: `boolean`

Whether to disable the date calendar of the 'date' control. Defaults to false.

___

### endControls

• `Optional` **endControls**: [`HfToolbarControl`](../modules/timegrid_model.md#hftoolbarcontrol)[]

The controls to be displayed at the end.

___

### startControls

• `Optional` **startControls**: [`HfToolbarControl`](../modules/timegrid_model.md#hftoolbarcontrol)[]

The controls to be displayed at the start.
