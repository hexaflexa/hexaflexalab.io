[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [timegrid-model](../modules/timegrid_model.md) / HfWeekdayInfo

# Class: HfWeekdayInfo

[timegrid-model](../modules/timegrid_model.md).HfWeekdayInfo

The weekday info model.

## Table of contents

### Constructors

- [constructor](timegrid_model.HfWeekdayInfo.md#constructor)

### Properties

- [defaultName](timegrid_model.HfWeekdayInfo.md#defaultname)
- [index](timegrid_model.HfWeekdayInfo.md#index)
- [name](timegrid_model.HfWeekdayInfo.md#name)
- [narrowName](timegrid_model.HfWeekdayInfo.md#narrowname)
- [shortName](timegrid_model.HfWeekdayInfo.md#shortname)

## Constructors

### constructor

• **new HfWeekdayInfo**(): [`HfWeekdayInfo`](timegrid_model.HfWeekdayInfo.md)

#### Returns

[`HfWeekdayInfo`](timegrid_model.HfWeekdayInfo.md)

## Properties

### defaultName

• **defaultName**: `string`

The default name of the weekday - in English.

___

### index

• **index**: `number`

The index of the weekday. Sunday - 0, Monday - 1, ..., Saturday - 6

___

### name

• **name**: `string`

The name of the weekday.

___

### narrowName

• **narrowName**: `string`

The index of the weekday.

___

### shortName

• **shortName**: `string`

The short name of the weekday.
