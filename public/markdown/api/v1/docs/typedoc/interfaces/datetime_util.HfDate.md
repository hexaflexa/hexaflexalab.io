[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [datetime-util](../modules/datetime_util.md) / HfDate

# Interface: HfDate

[datetime-util](../modules/datetime_util.md).HfDate

## Table of contents

### Properties

- [date](datetime_util.HfDate.md#date)
- [day](datetime_util.HfDate.md#day)
- [hours](datetime_util.HfDate.md#hours)
- [minutes](datetime_util.HfDate.md#minutes)
- [month](datetime_util.HfDate.md#month)
- [value](datetime_util.HfDate.md#value)
- [year](datetime_util.HfDate.md#year)

## Properties

### date

• **date**: `Date`

___

### day

• **day**: `number`

___

### hours

• **hours**: `number`

___

### minutes

• **minutes**: `number`

___

### month

• **month**: `number`

___

### value

• **value**: `string`

___

### year

• **year**: `number`
