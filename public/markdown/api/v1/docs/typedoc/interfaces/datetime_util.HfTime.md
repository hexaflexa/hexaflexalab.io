[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / [datetime-util](../modules/datetime_util.md) / HfTime

# Interface: HfTime

[datetime-util](../modules/datetime_util.md).HfTime

## Table of contents

### Properties

- [hours](datetime_util.HfTime.md#hours)
- [minutes](datetime_util.HfTime.md#minutes)
- [value](datetime_util.HfTime.md#value)

## Properties

### hours

• **hours**: `number`

___

### minutes

• **minutes**: `number`

___

### value

• **value**: `string`
