[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / datetime-util

# Module: datetime-util

## Table of contents

### Interfaces

- [HfDate](../interfaces/datetime_util.HfDate.md)
- [HfTime](../interfaces/datetime_util.HfTime.md)

### Functions

- [addDays](datetime_util.md#adddays)
- [concatDateTimeStrings](datetime_util.md#concatdatetimestrings)
- [dayMinutesToTimeString](datetime_util.md#dayminutestotimestring)
- [formatTime](datetime_util.md#formattime)
- [getDatePartString](datetime_util.md#getdatepartstring)
- [getNow](datetime_util.md#getnow)
- [getTimePartString](datetime_util.md#gettimepartstring)
- [getToday](datetime_util.md#gettoday)
- [getUTCDateTime](datetime_util.md#getutcdatetime)
- [isDateTimeValid](datetime_util.md#isdatetimevalid)
- [isDateValid](datetime_util.md#isdatevalid)
- [isTimeValid](datetime_util.md#istimevalid)
- [isToday](datetime_util.md#istoday)
- [localDateTimeToString](datetime_util.md#localdatetimetostring)
- [localDateToString](datetime_util.md#localdatetostring)
- [localTimeToString](datetime_util.md#localtimetostring)
- [parseDate](datetime_util.md#parsedate)
- [parseDateTime](datetime_util.md#parsedatetime)
- [parseTime](datetime_util.md#parsetime)
- [parseUTCDate](datetime_util.md#parseutcdate)
- [toDateString](datetime_util.md#todatestring)
- [toDateTimeString](datetime_util.md#todatetimestring)
- [utcDateTimeToString](datetime_util.md#utcdatetimetostring)
- [utcDateToString](datetime_util.md#utcdatetostring)
- [utcTimeToString](datetime_util.md#utctimetostring)

## Functions

### addDays

▸ **addDays**(`date`, `days`): `string`

Add a number of days to the date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `string` | the date |
| `days` | `number` | the number of days to be added |

#### Returns

`string`

the new date

___

### concatDateTimeStrings

▸ **concatDateTimeStrings**(`dateString`, `timeString`): `string`

Concatenate the date and time strings with a space delimiter.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `dateString` | `string` | the date string |
| `timeString` | `string` | the time string |

#### Returns

`string`

the concatenated date time string

___

### dayMinutesToTimeString

▸ **dayMinutesToTimeString**(`dayMinutes`): `string`

Get the time string in 'HH:mm' format from the day minutes.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `dayMinutes` | `number` | the day minutes |

#### Returns

`string`

the time string

___

### formatTime

▸ **formatTime**(`hours`, `minutes`): `string`

Format the time to a string in 'HH:mm' format.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `hours` | `number` | the hours |
| `minutes` | `number` | the minutes |

#### Returns

`string`

the time string

___

### getDatePartString

▸ **getDatePartString**(`dateString`): `string`

Get the date part string from the date string.
The accepted formats are: 'yyyy-MM-dd HH:mm', 'yyyy-MM-ddTHH:mm'

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `dateString` | `string` | the date string |

#### Returns

`string`

the date part string

___

### getNow

▸ **getNow**(): [`HfDate`](../interfaces/datetime_util.HfDate.md)

Get the datetime string of now in 'yyyy-MM-dd HH:mm' format.

#### Returns

[`HfDate`](../interfaces/datetime_util.HfDate.md)

the datetime string of now

___

### getTimePartString

▸ **getTimePartString**(`dateString`): `string`

Get the time part string from the date string.
The accepted formats are: 'yyyy-MM-dd HH:mm', 'yyyy-MM-ddTHH:mm'

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `dateString` | `string` | the date string |

#### Returns

`string`

the time part string

___

### getToday

▸ **getToday**(): `string`

Get the date string of today in 'yyyy-MM-dd' format.

#### Returns

`string`

the date string of today

___

### getUTCDateTime

▸ **getUTCDateTime**(`dateTime`): `Date`

Convert the string in 'yyyy-MM-dd HH:mm' format to an UTC Date object.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `dateTime` | `string` | the datetime string |

#### Returns

`Date`

the Date object

___

### isDateTimeValid

▸ **isDateTimeValid**(`dateTime`): `boolean`

Check date time has format 'yyyy-MM-dd HH:mm'.

#### Parameters

| Name | Type |
| :------ | :------ |
| `dateTime` | `string` |

#### Returns

`boolean`

___

### isDateValid

▸ **isDateValid**(`date`): `boolean`

Check date has format 'yyyy-MM-dd'.

#### Parameters

| Name | Type |
| :------ | :------ |
| `date` | `string` |

#### Returns

`boolean`

___

### isTimeValid

▸ **isTimeValid**(`time`): `boolean`

Check time has format 'HH:mm'.

#### Parameters

| Name | Type |
| :------ | :------ |
| `time` | `string` |

#### Returns

`boolean`

___

### isToday

▸ **isToday**(`date`): `boolean`

Check if the date is today.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `string` \| `Date` | the date to be checked |

#### Returns

`boolean`

true if the date is today, false otherwise

___

### localDateTimeToString

▸ **localDateTimeToString**(`date`, `timeString?`): `string`

Get the date time string in 'yyyy-MM-dd HH:mm' format from the local Date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `Date` | the date |
| `timeString?` | `string` | the time string |

#### Returns

`string`

the date time string

___

### localDateToString

▸ **localDateToString**(`date`): `string`

Get the date string in 'yyyy-MM-dd' format from the local Date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `Date` | the date |

#### Returns

`string`

the date string

___

### localTimeToString

▸ **localTimeToString**(`date`): `string`

Get the time string in 'HH:mm' format from the local Date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `Date` | the date |

#### Returns

`string`

the time string

___

### parseDate

▸ **parseDate**(`dateString`, `asUTC?`): [`HfDate`](../interfaces/datetime_util.HfDate.md)

Parse the date string in 'yyyy-MM-dd' format to an HfDate object.

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `dateString` | `string` | `undefined` | the date string |
| `asUTC` | `boolean` | `true` | - |

#### Returns

[`HfDate`](../interfaces/datetime_util.HfDate.md)

the UTC Date object

___

### parseDateTime

▸ **parseDateTime**(`dateTimeString`, `asUTC?`): [`HfDate`](../interfaces/datetime_util.HfDate.md)

Parse the date time string in 'yyyy-MM-dd HH:mm' format to an HfDate object.

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `dateTimeString` | `string` | `undefined` | the date time string |
| `asUTC` | `boolean` | `true` | - |

#### Returns

[`HfDate`](../interfaces/datetime_util.HfDate.md)

the UTC Date object

___

### parseTime

▸ **parseTime**(`timeString`): [`HfTime`](../interfaces/datetime_util.HfTime.md)

Parse the time string in 'HH:mm' format to an object with hours and minutes.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `timeString` | `string` | the time string |

#### Returns

[`HfTime`](../interfaces/datetime_util.HfTime.md)

the object with hours and minutes

___

### parseUTCDate

▸ **parseUTCDate**(`date`): `Date`

Convert the string in 'yyyy-MM-dd' format to an UTC Date object.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `string` | the date string |

#### Returns

`Date`

the Date object

___

### toDateString

▸ **toDateString**(`date`): `string`

Get the date string in 'yyyy-MM-dd' format from the date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | [`HfDate`](../interfaces/datetime_util.HfDate.md) | the date |

#### Returns

`string`

the date string

___

### toDateTimeString

▸ **toDateTimeString**(`dateTime`): `string`

Get the date time string in 'yyyy-MM-dd HH:mm' format from the date time.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `dateTime` | [`HfDate`](../interfaces/datetime_util.HfDate.md) | the date time |

#### Returns

`string`

the date time string

___

### utcDateTimeToString

▸ **utcDateTimeToString**(`date`, `timeString?`): `string`

Get the date time string in 'yyyy-MM-dd HH:mm' format from the UTC Date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `Date` | the date |
| `timeString?` | `string` | the time string |

#### Returns

`string`

the date time string

___

### utcDateToString

▸ **utcDateToString**(`date`): `string`

Get the date string in 'yyyy-MM-dd' format from the UTC Date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `Date` | the date |

#### Returns

`string`

the date string

___

### utcTimeToString

▸ **utcTimeToString**(`date`): `string`

Get the time string in 'HH:mm' format from the UTC Date.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `date` | `Date` | the date |

#### Returns

`string`

the time string
