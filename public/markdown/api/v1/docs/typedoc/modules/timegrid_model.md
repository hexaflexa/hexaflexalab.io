[@hexaflexa/timegrid](../README.md) / [Modules](../modules.md) / timegrid-model

# Module: timegrid-model

## Table of contents

### Classes

- [HfBodyConfig](../classes/timegrid_model.HfBodyConfig.md)
- [HfDaysConfig](../classes/timegrid_model.HfDaysConfig.md)
- [HfEvent](../classes/timegrid_model.HfEvent.md)
- [HfEventConfig](../classes/timegrid_model.HfEventConfig.md)
- [HfEventDragResize](../classes/timegrid_model.HfEventDragResize.md)
- [HfHeaderDayConfig](../classes/timegrid_model.HfHeaderDayConfig.md)
- [HfHeaderResourceConfig](../classes/timegrid_model.HfHeaderResourceConfig.md)
- [HfResource](../classes/timegrid_model.HfResource.md)
- [HfResourceAvatarConfig](../classes/timegrid_model.HfResourceAvatarConfig.md)
- [HfTimegridConfig](../classes/timegrid_model.HfTimegridConfig.md)
- [HfTimegridEventChangedEvent](../classes/timegrid_model.HfTimegridEventChangedEvent.md)
- [HfTimegridEventDragResizeStateChangedEvent](../classes/timegrid_model.HfTimegridEventDragResizeStateChangedEvent.md)
- [HfToolbarConfig](../classes/timegrid_model.HfToolbarConfig.md)
- [HfWeekdayInfo](../classes/timegrid_model.HfWeekdayInfo.md)

### Type Aliases

- [HfEventDisplay](timegrid_model.md#hfeventdisplay)
- [HfEventResizeEdges](timegrid_model.md#hfeventresizeedges)
- [HfEventSource](timegrid_model.md#hfeventsource)
- [HfToolbarControl](timegrid_model.md#hftoolbarcontrol)

## Type Aliases

### HfEventDisplay

Ƭ **HfEventDisplay**: ``"default"`` \| ``"none"``

The event display type.

___

### HfEventResizeEdges

Ƭ **HfEventResizeEdges**: ``"both"`` \| ``"start"`` \| ``"end"``

The event resize action.

___

### HfEventSource

Ƭ **HfEventSource**: (`dates`: `string`[], `callback`: (`events`: [`HfEvent`](../classes/timegrid_model.HfEvent.md)[]) => `void`, `failureCallback`: (`error`: `any`) => `void`) => `void`

The event source function signature. The events should be passed to the callback.

#### Type declaration

▸ (`dates`, `callback`, `failureCallback`): `void`

##### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `dates` | `string`[] | the dates for which to fetch the events |
| `callback` | (`events`: [`HfEvent`](../classes/timegrid_model.HfEvent.md)[]) => `void` | the callback to be called with the fetched events |
| `failureCallback` | (`error`: `any`) => `void` | the callback to be called in case of failure |

##### Returns

`void`

___

### HfToolbarControl

Ƭ **HfToolbarControl**: ``"prev"`` \| ``"next"`` \| ``"today"`` \| ``"date"`` \| ``"loading"``

The toolbar controls.
