# Utility functions

The utility functions provided by the `hf-timegrid` web component:

```typescript
export interface HfTime {
  hours: number;
  minutes: number;
  value: string;
}

export interface HfDate {
  year: number;
  month: number;
  day: number;
  hours: number;
  minutes: number;
  value: string;
  date: Date;
}

/**
 * Check date has format 'yyyy-MM-dd'.
 */
export const isDateValid = (date: string): boolean => {
  return /^\d{4}-\d{2}-\d{2}$/.test(date);
}

/**
 * Check time has format 'HH:mm'.
 */
export const isTimeValid = (time: string): boolean => {
  return /^\d{2}:\d{2}$/.test(time);
}

/**
 * Check date time has format 'yyyy-MM-dd HH:mm'.
 */
export const isDateTimeValid = (dateTime: string): boolean => {
  return /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}$/.test(dateTime);
}

/**
 * Parse the time string in 'HH:mm' format to an object with hours and minutes.
 * @param timeString the time string
 * @returns the object with hours and minutes
 */
export const parseTime = (timeString: string): HfTime => {
  try {
    const parts = timeString.split(':');
    const hours = parseInt(parts[0]);
    const minutes = parseInt(parts[1]);
    return { hours: hours, minutes: minutes, value: timeString };  
  } catch (error) {
    return { hours: 0, minutes: 0, value: '00:00'};
  }
}

/**
 * Parse the date string in 'yyyy-MM-dd' format to an HfDate object.
 * @param dateString the date string
 * @returns the UTC Date object
 */
export const parseDate = (dateString: string, asUTC: boolean = true): HfDate => {
  try {
    const dateParts = dateString.split(' ');
    const parts = dateParts[0].split('-');
    const year = parseInt(parts[0]);
    const month = parseInt(parts[1]);
    const day = parseInt(parts[2]);
    return {
      year: year,
      month: month,
      day: day,
      hours: 0,
      minutes: 0,
      value: dateString,
      date: asUTC ? new Date(Date.UTC(year, month - 1, day)) : new Date(year, month - 1, day)
    };
  } catch (error) {
    return {
      year: 1970,
      month: 0,
      day: 1,
      hours: 0,
      minutes: 0,
      value: '1970-01-01',
      date: asUTC ? new Date(Date.UTC(1970, 0, 1)) : new Date(1970, 0, 1)
    };
  }
}

/**
 * Parse the date time string in 'yyyy-MM-dd HH:mm' format to an HfDate object.
 * @param dateTimeString the date time string
 * @returns the UTC Date object
 */
export const parseDateTime = (dateTimeString: string, asUTC: boolean = true): HfDate => {
  try {
    const parts = dateTimeString.split(' ');
    const date = parseDate(parts[0], asUTC);
    const time = parseTime(parts[1]);
    return {
      year: date.year,
      month: date.month,
      day: date.day,
      hours: time.hours,
      minutes: time.minutes,
      value: dateTimeString,
      date: asUTC ? new Date(Date.UTC(date.year, date.month - 1, date.day, time.hours, time.minutes)) : new Date(date.year, date.month - 1, date.day, time.hours, time.minutes)
    };
  } catch (error) {
    return {
      year: 1970,
      month: 0,
      day: 1,
      hours: 0,
      minutes: 0,
      value: '1970-01-01 00:00',
      date: asUTC ? new Date(Date.UTC(1970, 0, 1, 0, 0)) : new Date(1970, 0, 1, 0, 0)
    };
  }
}

/**
 * Convert the string in 'yyyy-MM-dd' format to an UTC Date object.
 * @param date the date string
 * @returns the Date object
 */
export const parseUTCDate = (date: string): Date => {
  const hfDate = parseDate(date);
  return hfDate.date;
}

/**
 * Convert the string in 'yyyy-MM-dd HH:mm' format to an UTC Date object.
 * @param dateTime the datetime string
 * @returns the Date object
 */
export const getUTCDateTime = (dateTime: string): Date => {
  const hfDateTime = parseDateTime(dateTime);
  return hfDateTime.date;
}

/**
 * Format the time to a string in 'HH:mm' format.
 * @param hours the hours
 * @param minutes the minutes
 * @returns the time string
 */
export const formatTime = (hours: number, minutes: number): string => {
  return (hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes);
}

/**
 * Get the date string in 'yyyy-MM-dd' format from the date.
 * @param date the date
 * @returns the date string
 */
export const toDateString = (date: HfDate): string => {
  return `${date.year}-${date.month < 10 ? '0' + date.month : date.month}-${date.day < 10 ? '0' + date.day : date.day}`;
}

/**
 * Get the date time string in 'yyyy-MM-dd HH:mm' format from the date time.
 * @param dateTime the date time
 * @returns the date time string
 */
export const toDateTimeString = (dateTime: HfDate): string => {
  return `${toDateString(dateTime)} ${formatTime(dateTime.hours, dateTime.minutes)}`;
}

/**
 * Concatenate the date and time strings with a space delimiter.
 * @param dateString the date string
 * @param timeString the time string
 * @returns the concatenated date time string
 */
export const concatDateTimeStrings = (dateString: string, timeString: string): string => {
  return `${dateString} ${timeString}`;
}

/**
 * Get the date part string from the date string.
 * The accepted formats are: 'yyyy-MM-dd HH:mm', 'yyyy-MM-ddTHH:mm'
 * @param dateString the date string
 * @returns the date part string
 */
export const getDatePartString = (dateString: string): string => {
  if (dateString.indexOf('T') < 0) {
    return dateString.split(' ')[0];
  } else {
    return dateString.split('T')[0];
  }
}

/**
 * Get the time part string from the date string.
 * The accepted formats are: 'yyyy-MM-dd HH:mm', 'yyyy-MM-ddTHH:mm'
 * @param dateString the date string
 * @returns the time part string
 */
export const getTimePartString = (dateString: string): string => {
  if (dateString.indexOf('T') < 0) {
    return dateString.split(' ')[1];
  } else {
    return dateString.split('T')[1];
  }
}

/**
 * Get the time string in 'HH:mm' format from the day minutes.
 * @param dayMinutes the day minutes
 * @returns the time string
 */
export const dayMinutesToTimeString = (dayMinutes: number): string => {
  const hours = Math.floor(dayMinutes / 60);
  const minutes = dayMinutes % 60;
  return `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}`;
}

/**
 * Add a number of days to the date.
 * @param date the date
 * @param days the number of days to be added
 * @returns the new date
 */
export const addDays = (date: string, days: number): string => {
  const parsedDate = parseDate(date);
  const result = parsedDate.date;
  result.setUTCDate(result.getUTCDate() + days);
  return utcDateToString(result);
};

/**
 * Check if the date is today.
 * @param date the date to be checked
 * @returns true if the date is today, false otherwise
 */
export const isToday = (date: string | Date): boolean => {
  if (typeof date === 'string') {
    return getToday() === date;
  } else {
    const today = new Date();
    return date.getUTCFullYear() == today.getUTCFullYear() && date.getUTCMonth() == today.getUTCMonth() && date.getUTCDate() == today.getUTCDate();
  }
}

/**
 * Get the date string of today in 'yyyy-MM-dd' format.
 * @returns the date string of today
 */
export function getToday(): string {
  const today = new Date();
  today.setUTCHours(0, 0, 0, 0);
  return utcDateToString(today);
}

/**
 * Get the datetime string of now in 'yyyy-MM-dd HH:mm' format.
 * @returns the datetime string of now
 */
export function getNow(): HfDate {
  return parseDateTime(utcDateTimeToString(new Date()));
}

/**
 * Get the date string in 'yyyy-MM-dd' format from the local Date.
 * @param date the date
 * @returns the date string
 */
export const localDateToString = (date: Date): string => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const dayOfMonth = date.getDate();
  return `${year}-${month < 10 ? '0' + month : month}-${dayOfMonth < 10 ? '0' + dayOfMonth : dayOfMonth}`;
}

/**
 * Get the time string in 'HH:mm' format from the local Date.
 * @param date the date
 * @returns the time string
 */
export const localTimeToString = (date: Date): string => {
  const hours = date.getHours();
  const minutes = date.getMinutes();
  return `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}`;
}

/**
 * Get the date time string in 'yyyy-MM-dd HH:mm' format from the local Date.
 * @param date the date
 * @param timeString the time string
 * @returns the date time string
 */
export const localDateTimeToString = (date: Date, timeString?: string): string => {
  const dateString = localDateToString(date);
  if (timeString) {
    return `${dateString} ${timeString}`;
  } else {
    return `${dateString} ${localTimeToString(date)}`;
  }
}

/**
 * Get the date string in 'yyyy-MM-dd' format from the UTC Date.
 * @param date the date
 * @returns the date string
 */
export const utcDateToString = (date: Date): string => {
  const year = date.getUTCFullYear();
  const month = date.getUTCMonth() + 1;
  const dayOfMonth = date.getUTCDate();
  return `${year}-${month < 10 ? '0' + month : month}-${dayOfMonth < 10 ? '0' + dayOfMonth : dayOfMonth}`;
};

/**
 * Get the time string in 'HH:mm' format from the UTC Date.
 * @param date the date
 * @returns the time string
 */
export const utcTimeToString = (date: Date): string => {
  const hours = date.getUTCHours();
  const minutes = date.getUTCMinutes();
  return `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}`;
}

/**
 * Get the date time string in 'yyyy-MM-dd HH:mm' format from the UTC Date.
 * @param date the date
 * @param timeString the time string
 * @returns the date time string
 */
export const utcDateTimeToString = (date: Date, timeString?: string): string => {
  const dateString = utcDateToString(date);
  if (timeString) {
    return `${dateString} ${timeString}`;
  } else {
    return `${dateString} ${utcTimeToString(date)}`;
  }
};
```